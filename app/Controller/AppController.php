	<?php
	/**
	 * Application level Controller
	 *
	 * This file is application-wide controller file. You can put all
	 * application-wide controller-related methods here.
	 *
	 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
	 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
	 *
	 * Licensed under The MIT License
	 * For full copyright and license information, please see the LICENSE.txt
	 * Redistributions of files must retain the above copyright notice.
	 *
	 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
	 * @link          http://cakephp.org CakePHP(tm) Project
	 * @package       app.Controller
	 * @since         CakePHP(tm) v 0.2.9
	 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
	 */

	App::uses('Controller', 'Controller');

	/**
	 * Application Controller
	 *
	 * Add your application-wide methods in the class below, your controllers
	 * will inherit them.
	 *
	 * @package		app.Controller
	 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
	 */
	class AppController extends Controller {

		public $helpers = array('Html','Form','Session');
		public $components = array(
			'DebugKit.Toolbar',
			'Paginator',
			'Flash',
			'Session',
			'Auth' => array(
				'authenticate' => array(
					'Form' => array(
						'passwordHasher' => 'Blowfish'
					)
				),
//                            'loginAction' => array(
//                                        'controller' => 'users',
//                                        'action' => 'login',
//                                        'plugin' => 'admin'),
			    'logoutRedirect' => '/',
			    'loginRedirect' => '/',
//				'authorize' => array(
//					'Actions' => array('actionPath' => 'controllers')
//				),
//                            'Acl'
			)
		);

		public function isAuthorized($users) {
			if (isset($users['group_id']) && $users['group_id'] == '1') {
                        return true;
                    }
                    return false;
		}
		
		// public function get_user() {
		// 	if ($this->Auth->login()){
		// 		return $this->Auth->user();
		// 	}
		// }

	    public function beforeFilter()
	    {
	     	$this->Auth->authorize = array(
			     AuthComponent::ALL => array('actionPath' => 'controllers/'),
			     'Actions',
			     'Controller'
			 );
	    	// $this->set('user_info', $this->get_user());

			$this->Auth->allow();

			$this->loadModel('Faq');
			$faqs = $this->Faq->find('all');
			$this->set('faqs', $faqs);

			$this->loadModel('Setting');

			$deli = $this->Setting->find("first", array('conditions' => array('name' => 'delivery_shipping')));
					$this->set('deli', $deli);
			
			$phone1 = $this->Setting->find("first", array('conditions' => array('name' => 'contact_phone1')));
					$this->set('phone1', $phone1);
			
			$phone2 = $this->Setting->find("first", array('conditions' => array('name' => 'contact_phone2')));
					$this->set('phone2', $phone2);
			
			$email = $this->Setting->find("first", array('conditions' => array('name' => 'contact_mail')));
					$this->set('email', $email);
			
			$addr = $this->Setting->find("first", array('conditions' => array('name' => 'contact_address')));
					$this->set('addr', $addr);
			
			$shopping = $this->Setting->find("first", array('conditions' => array('name' => 'shoping_guide')));
					$this->set('shopping', $shopping);

			$return = $this->Setting->find("first", array('conditions' => array('name' => 'return_policy')));
					$this->set('return', $return);

			$open = $this->Setting->find("first", array('conditions' => array('name' => 'Open_hour')));
					$this->set('open', $open);

                        $close = $this->Setting->find("first", array('conditions' => array('name' => 'Close_hour')));
                                                    $this->set('close', $close);

                        $open_weekend = $this->Setting->find("first", array('conditions' => array('name' => 'open_weekend')));
                                                    $this->set('open_weekend', $open_weekend);

                        $close_weekend = $this->Setting->find("first", array('conditions' => array('name' => 'close_weekend')));
                                                    $this->set('close_weekend', $close_weekend);
                        $site_name = $this->Setting->find("first", array('conditions' => array('name' => 'site_name')));
			$this->set('site_name', $site_name);
                        
			$this->loadModel('Product');
				$sql = array('order' => array('rating'=>'desc'),
                                             'limit' => 5,
                                             'conditions' => array( 'Product.published' => 1)
                                    );
				$product_aside = $this->Product->find('all',$sql);
				$this->set('product_aside', $product_aside);
                        $sql3 = array('order' => 'rand()',
                                             'limit' => 6,
                                             'conditions' => array( 'Product.published' => 1)
                                    );
				$product_footer = $this->Product->find('all',$sql3);
				$this->set('product_footer', $product_footer);        
                $sql2 = "select max(price) from products";
                $max_price = $this->Product->query($sql2);
                $this->set('max_price', $max_price);
                $product_slide = $this->Product->find('all',
                	array(
            			'AND' => array(
	                		'order' =>array(
	                			'rating' => 'desc',
                			),
	                		'order' =>array(
	                			'recommended' => 'desc'
	                		)
                		),
                		'recursive' => -1,
                		'limit' => 5
                            )
                	);
                $this->set(compact('product_slide'));

			$this->loadModel('Setting');
                $setting = $this->Setting->find('all');
                $this->set('setting', $setting);

            $this->loadModel('Category');
	            $cate_aside = $this->Category->find('all');
	            $this->set('cate_aside', $cate_aside);
        }
	public function sum($cart) {
		$pricetotal = 0;
		if($cart){
			foreach ($cart as $products) {
				$pricetotal += $products['prices'] * $products['quantity'];
			}
			return $pricetotal;
		}else{
			return null;
		}
	}
        public function sum_quantity($cart) {
		$quan = 0;
		if($cart){
			foreach ($cart as $products) {
				$quan += $products['quantity'];
			}
			return $quan;
		}else{
			return null;
		}
	}

	public function between($date, $start, $end, $timezone = 'Europe/London'){
		date_default_timezone_set($timezone);
		$date = strtotime($date);
		$start = strtotime($start);
		$end = strtotime($end);
		if ($date >= $start && $date <= $end) {
			return true;
		}else{
			return false;
		}
	}

}
