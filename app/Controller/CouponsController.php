<?php
/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class CouponsController extends AppController {
/**
* index method
* @return void
*/
	public function index() {
		$this->layout = false;
		$coupons = $this->Coupon->find('all');
		$this->set(compact('coupons'));
	}
/**
* view method
* @return void
*/
	public function view($id = null) {
		if (!$this->Coupon->exists($id)) {
			throw new NotFoundException(__('Invailid coupon'));
		}
		$options = array('condition' => array('Coupon.'.$this->Coupon->primaryKey => $id));
		$this->set('coupon', $this->Coupon->find('first', $options));
	}
/**
* view method
* @return void
*/
	public function add() {
		if ($this->request->is('post')) {
			$code = $this->request->data['Coupon']['code'];
			$coupon = $this->Coupon->findByCode($code);
			if (!empty($coupon)) {
				$today = date('Y-m-d H:i:s');
				if ($this->between($today, $coupon['Coupon']['time_start'], $coupon['Coupon']['time_end'])) {
					$this->Session->write('payment.coupon', $coupon['Coupon']['code']);
					$this->Session->write('payment.discount', $coupon['Coupon']['percent']);
					$total = $this->Session->read('payment.total');
					$pay = $total - $total*$coupon['Coupon']['percent']/100;
					$this->Session->write('payment.pay', $pay);
				}else{
					$this->Session->setFlash('Discount code is expired use!', 'default', array('class' => 'alert alert-danger'), 'coupon');
				}
			}else{
				$this->Session->setFlash('Discount code is invailid!', 'default', array('class' => 'alert alert-danger'), 'coupon');
			}
			$this->redirect($this->referer());
		}
	}
}