<?php
App::uses('AppController', 'Controller');

class OrdersController extends AppController {

	public function checkout(){
		if ($this->request->is('post')) {
			$order = $this->request->data('Order');
			$this->Session->write('order', $order);
			$this->Session->setFlash('Submit order succesful!', 'default', array('class' => 'alert alert-danger'), 'addorder');
			return $this->redirect($this->referer());
		}
		$this->Session->setFlash('Submit order failed!', 'default', array('class' => 'alert alert-danger'), 'addorder');
		return $this->redirect($this->referer());
	}

    public function order(){
		if(!empty($this->Session->read('cart'))){
	    	if (AuthComponent::user() || (!empty($this->Session->read('order')))){
		        $data = array(
		                'user_id' => AuthComponent::user()['id'],
		                'customer_info' => json_encode($this->Session->read('order')),
		                'order_info' => json_encode($this->Session->read('cart')),
		                'payment_info' => json_encode($this->Session->read('payment')),
		                'status' => 0
		                );
		        if ($this->Order->saveAll($data)){
		            $this->Session->delete('cart');
		            $this->Session->delete('payment');
		            $this->Session->delete('order');
		            return $this->redirect($this->referer());
		        }else{
		            $this->Session->setFlash('Ok!', 'default', array('class' => 'alert alert-danger'), 'addorder');
		            return $this->redirect($this->referer());
		        }
		    }
		    $this->Session->setFlash('You must login or submit form nearby!', 'default', array('class' => 'alert alert-danger'), 'addorder');
	        return $this->redirect($this->redirect('/products/check_out'));
	    }
	    $this->Session->setFlash('Đã mua gì đâu!', 'default', array('class' => 'alert alert-danger'), 'addorder');
        return $this->redirect($this->referer());
    }
}