<?php

App::uses ('AppController','Controller');

class UsersController extends AppController {
    // public $name = 'Users';
    // public $helpers = array('Html','Form','Session');
    // public $components = array('Paginator', 'DebugKit.Toolbar', 'Session');



	public function index() {
        $id = $this->Auth->user()['id'];
        if (empty($id)) {
            throw new NotFoundException(__('The User does not exist!'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $users = $this->User->find('first', $options);
        $this->set('users', $users);
	}
    public function login() {
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $this->Flash->success(__('Logged in successfully!'));
                return $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Session->setFlash(__('Username or password is incorrect! Please, try again', 'default', array('class'=>'alert alert-danger'),'login'));
                return $this->redirect($this->referer());
            }
        }
    }

    public function logout(){
        $this->redirect($this->Auth->logout());
    }
    private function upload($img,$img_name){
        $file = new File($img);
        $file_name = $img_name;
        if($file->copy(APP.'/webroot/images/avatar/'.$file_name)){
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function register() {
        if ($this->request->is('post')) {
            $img = $this->request->data['User']['avatar']['tmp_name'];
            $img_name = $this->request->data['User']['avatar']['name'];
            $result = $this->upload($img,$img_name);
            if($result){
                $this->request->data['User']['avatar'] = $img_name;
                if (($this->data['User']['password']) == ($this->data['User']['confirm_password'])){
                    $this->User->create();
                    if ($this->User->save($this->request->data)) {
                        $this->Session->setFlash(__('You has been create an account successfully!', 'default', array('class'=>'alert alert-register')));
                        $this->Auth->login();
                        return $this->redirect('/');
                    }else{
                        $this->Session->setFlash(__('Account could not be create. Please, try again!','default', array('class'=>'alert alert-danger')));
                    }
                }
                $this->Session->setFlash(__("Password confirmation doesn't match Password! Please, try again!",'default', array('class'=>'alert alert-danger')));
            }
        }
    }

    public function edit($id = null) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $length = 4;
                $rand1 = substr( str_shuffle( $characters ), 0, $length );
                $rand2 = substr( str_shuffle( $characters ), 0, $length );
            $rand = $rand1.'-'.$rand2;
        
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid User'));
        }

        $user = $this->User->findById($id);
        if (!$user) {
            throw new NotFoundException(__('Invalid User'));
        }

        if ($this->request->is(array('post', 'put'))) {
            if (!empty($this->request->data['User']['avatar_new'])) {
                $img = $this->request->data['User']['avatar_new']['tmp_name'];
                $img_name = $rand.'-'.$this->request->data['User']['avatar_new']['name'];
            if($this->upload($img,$img_name)){
                $file = new File(APP.'/webroot/images/avatar/'.$this->request->data['User']['avatar']);
                $file->delete();
                    $this->request->data['User']['avatar'] = $img_name;
                    
                }
            } else {
                unset($this->request->data['User']['avatar']);
            }
            $this->User->id = $id;
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('The User has been updated.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to update The User.'));
        }

        if (!$this->request->data) {
            $this->request->data = $user;
        }
        $this->set('users', $user);
    }


	public function delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid User'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Flash->success(__('The user has been deleted.'));
		} else {
			$this->Flash->error(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
