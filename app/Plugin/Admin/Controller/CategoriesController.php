<?php

App::uses('Admin.AdminAppController', 'Controller');
App::uses('File', 'Utility');

class CategoriesController extends AdminAppController {

    public $components = array('Paginator', 'Flash');
    

    public function index() {
        $this->paginate = array('limit' => 10);
        $categories = $this->paginate('Category');
        // Search
        if ($this->request->is('post')) {
            $keyword = $this->request->data['Category']['keyword'];
            $categories = $this->Category->find('all', array(
                'fields' => array('id', 'name', 'image', 'info',),
                'conditions' => array(
                    'OR' => array('name like' => '%' . $keyword . '%',
                        'info like' => '%' . $keyword . '%'))
            ));
        }
            $this->set('categories', $categories);
    }

    public function view($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid Category'));
        }

        $categories = $this->Category->findById($id);
        if (!$categories) {
            throw new NotFoundException(__('Invalid Category'));
        }
        $this->set('categories', $categories);
    }

    public function add() {
        if ($this->request->is('post')) {
            $img = $this->request->data['Category']['image']['tmp_name'];
            $img_name = $this->request->data['Category']['image']['name'];
            $result = $this->upload($img,$img_name);
            if($result){
                $this->request->data['Category']['image'] = $img_name;
                $this->Category->create();
                if ($this->Category->save($this->request->data)) {
                    $this->Session->setFlash(__('The Category has been saved.'));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Flash->error(__('The Category could not be saved. Please, try again.'));
                }
            }
        }
//		$this->set('Category', $categories);
    }

    public function delete($id = null) {
        $this->Category->id = $id;
        if (!$this->Category->exists()) {
            throw new NotFoundException(__('Invalid category'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Category->delete()) {
            $this->Flash->success(__('The category has been deleted.'));
        } else {
            $this->Flash->error(__('The category could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }
    private function upload($img,$img_name){
        $file = new File($img);
        $file_name = $img_name;
        if($file->copy(APP.'/webroot/images/'.$file_name)){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function edit($id = null) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $length = 4;
                $rand1 = substr( str_shuffle( $characters ), 0, $length );
                $rand2 = substr( str_shuffle( $characters ), 0, $length );
            $rand = $rand1.'-'.$rand2;
       
        $categories = $this->Category->findById($id);
        if (!$categories) {
            throw new NotFoundException(__('Invalid category'));
        }

        if ($this->request->is(array('post', 'put'))) {
            if (!empty($this->request->data['Category']['image_new'])) {
                $img = $this->request->data['Category']['image_new']['tmp_name'];
                $img_name = $rand.'-'.$this->request->data['Category']['image_new']['name'];
                if($this->upload($img,$img_name)){
                    $file = new File(APP.'/webroot/images/'.$this->request->data['Category']['image']);
                    $file->delete();
                    $this->request->data['Category']['image'] = $img_name;
                }
            } else {
                unset($this->request->data['Category']['image']);
            }
            $this->request->data['Category']['id'] = $id;
            if ($this->Category->save($this->request->data)) {
                $this->Flash->success(__('The category has been updated.'));
                return $this->redirect(array('action' => 'index'));
            }

            $this->Flash->error(__('Unable to update The category.'));
        }

        if (!$this->request->data) {
            $this->request->data = $categories;
        }
        $this->set('categories', $categories);
    }


}
