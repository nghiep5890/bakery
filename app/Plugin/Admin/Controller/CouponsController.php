<?php

/**
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');

/**
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class CouponsController extends AdminAppController {

    /**
     * index method
     * @return void
     */
    public function index() {
        $this->paginate = array('limit' => 10);
        $coupons = $this->paginate('Coupon');
        
        // Search
        if ($this->request->is('post')) {
            $keyword = $this->request->data['Coupon']['keyword'];
            $coupons = $this->Coupon->find('all', array(
                'conditions' => array(
                    'OR' => array('code like' => '%' . $keyword . '%',
                        'percent like' => '%' . $keyword . '%'))
            ));
            $this->set('coupons', $coupons);
        }
        $this->set('coupons', $coupons);
    }

    /**
     * view method
     * @return void
     */
    public function view($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid Coupon'));
        }

        $coupons = $this->Coupon->findById($id);
        if (!$coupons) {
            throw new NotFoundException(__('Invalid Coupon'));
        }
        $this->set('coupons', $coupons);
    }

    public function add() {
        if ($this->request->is('post')) {

            // $this->request->data['Catagory']['slug'] = $this->Tool->slug($this->request->data['Catagory']['name']);
            $this->Coupon->create();
            if ($this->Coupon->save($this->request->data)) {
                $this->Session->setFlash(__('The Coupon has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The Coupon could not be saved. Please, try again.'));
            }
        }
//		$this->set('Category', $categories);
    }

    public function delete($id = null) {
        $this->Coupon->id = $id;
        if (!$this->Coupon->exists()) {
            throw new NotFoundException(__('Invalid Coupon'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Coupon->delete()) {
            $this->Flash->success(__('The Coupon has been deleted.'));
        } else {
            $this->Flash->error(__('The Coupon could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function edit($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid category'));
        }

        $coupons = $this->Coupon->findById($id);
        if (!$coupons) {
            throw new NotFoundException(__('Invalid Coupon'));
        }

        if ($this->request->is(array('post', 'put'))) {

            $this->request->data['Coupon']['id'] = $id;
            if ($this->Coupon->save($this->request->data)) {
                $this->Flash->success(__('The Coupon has been updated.'));
                return $this->redirect(array('action' => 'index'));
            }

            $this->Flash->error(__('Unable to update The Coupon.'));
        }

        if (!$this->request->data) {
            $this->request->data = $coupons;
        }
        $this->set('coupons', $coupons);
    }

}
