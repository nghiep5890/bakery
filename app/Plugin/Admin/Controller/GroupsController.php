<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Admin.AdminAppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class GroupsController extends AdminAppController {

    public function index(){
        $groups = $this->Group->find('all');
        $this->set(compact('groups'));
    }
    public function add() {
        if ($this->request->is('post')) {
            $this->Group->create();
            if ($this->Group->save($this->request->data)) {
                $this->Session->setFlash(__('The Group has been saved.'));
                return $this->redirect(array('action' => 'index '));
            } else {
                $this->Flash->error(__('The Group could not be saved. Please, try again.'));
            }
        }
    }
    public function delete($id = null) {
        $this->Group->id = $id;
        if (!$this->Group->exists()) {
            throw new NotFoundException(__('Invalid Group'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Group->delete()) {
            $this->Flash->success(__('The Group has been deleted.'));
        } else {
            $this->Flash->error(__('The Group could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }
    public function edit($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid groups'));
        }else{

            $groups = $this->Group->findById($id);
            if (!$groups) {
                throw new NotFoundException(__('Invalid groups'));
            }else{

                if ($this->request->is(array('post', 'put'))) {
                    $this->Group->id = $id;
                    if ($this->Group->save($this->request->data)) {
                        $this->Flash->success(__('The Group has been updated.'));
                        return $this->redirect(array('action' => 'index'));
                    }
                    $this->Flash->error(__('Unable to update The Group.'));
                }

                if (!$this->request->data) {
                    $this->request->data = $groups;
                }
            }
            $this->set('groups', $groups);
        }
    }
    // public function create_aros() {
    //     $aro = $this->Acl->Aro;
    //     $groups = $this->Group->find('all');
    //     foreach ($groups as $group) {
    //         $aro->create();
    //         $aro->save(array(
    //             'alias' => $group['Group']['name'],
    //             'model' => 'Group',
    //             'foreign_key' => $group['Group']['id']
    //             ));
    //     }

    //     $this->loadModel('User');
    //     $users = $this->User->find('all');
    //     foreach ($users as $user) {
    //         $aro->create();
    //         $aro->save(array(
    //             'parent_id' => $user['User']['group_id'],
    //             'alias' => $user['User']['username'],
    //             'model' => 'User',
    //             'foreign_key' => $user['User']['id']
    //             ));
    //     }
    //     exit;
    // }
}
