<?php

App::uses('Admin.AdminAppController', 'Controller');

class OrdersController extends AdminAppController {


    public $components = array('Paginator', 'Flash');

    public function index() {
        $this->loadModel('Order');
        $this->paginate = array('limit' => 10);
        $orders = $this->paginate('Order');
        // Search
        if ($this->request->is('post')) {
            $keyword = $this->request->data['Order']['keyword'];
            $orders = $this->Order->find('all', array(
                'conditions' => array(
                    'OR' => array('Order.id like' => '%' . $keyword . '%',
                        'User.username like' => '%' . $keyword . '%',
                        'Order.customer_info like' => '%' . $keyword . '%'))
                ));
        }
        $this->set('orders', $orders);
    }
    public function view($id = null) {
        $this->loadModel('Order');

        if (!$id) {
            throw new NotFoundException(__('Invalid order'));
        }

        $orders = $this->Order->findById($id);
        if (!$orders) {
            throw new NotFoundException(__('Invalid orders'));
        }
        $this->set('orders', $orders);
    }
    public function delete($id = null) {
        $this->Order->id = $id;
        if (!$this->Order->exists()) {
            throw new NotFoundException(__('Invalid Order'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Order->delete()) {
            $this->Flash->success(__('The Order has been deleted.'));
        } else {
            $this->Flash->error(__('The Order could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }
}