<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Admin.AdminAppController', 'Controller');
App::uses('File', 'Utility');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ProductsController extends AdminAppController {

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array();

    /**
     * Displays a view
     *
     * @return void
     * @throws NotFoundException When the view file could not be found
     * 	or MissingViewException in debug mode.
     */
    public function index() {
        $this->paginate = array('limit' => 10);
        $product = $this->paginate('Product');

        // Search
        if ($this->request->is('post')) {
            $keyword = $this->request->data['Product']['keyword'];
            $product = $this->Product->find('all', array(
                'fields' => array('id', 'name', 'path_img1', 'description', 'created'),
                'conditions' => array(
                    'OR' => array('name like' => '%' . $keyword . '%',
                        'description like' => '%' . $keyword . '%'))
            ));
        }
        $this->set('product', $product);
    }

    public function list_product() {
        $product = $this->Product->find('all', array(
                // 'recursive' => -1
                )
        );

        // pr($product);exit;
        $this->set('product', $product);
    }

    public function add() {
        $this->loadModel('Category');
        $category = $this->Category->find('list');
        $this->set('category', $category);
        if ($this->request->is('post')) {
            $img1 = $this->request->data['Product']['path_img1']['tmp_name'];
            $img_name1 = $this->request->data['Product']['path_img1']['name'];
            $result1 = $this->upload($img1,$img_name1);
            
            $img2 = $this->request->data['Product']['path_img2']['tmp_name'];
            $img_name2 = $this->request->data['Product']['path_img2']['name'];
            $result2 = $this->upload($img2,$img_name2);
            
            $img3 = $this->request->data['Product']['path_img3']['tmp_name'];
            $img_name3 = $this->request->data['Product']['path_img3']['name'];
            $result3 = $this->upload($img3,$img_name3);
            
            $img4 = $this->request->data['Product']['path_img4']['tmp_name'];
            $img_name4 = $this->request->data['Product']['path_img4']['name'];
            $result4 = $this->upload($img4,$img_name4);
            
            if($result1||$result2||$result3||$result4){
                $this->request->data['Product']['path_img1'] = $img_name1;
                $this->request->data['Product']['path_img2'] = $img_name2;
                $this->request->data['Product']['path_img3'] = $img_name3;
                $this->request->data['Product']['path_img4'] = $img_name4;
                $this->Product->create();
                if ($this->Product->save($this->request->data)) {
                    $this->Flash->success(__('The Product has been saved.'));
                    return $this->redirect(array('action' => '/add'));
                } else {
                    $this->Flash->error(__('The blog could not be saved. Please, try again.'));
                }
            }
        }
    }

    public function delete($id = null) {
        $this->Product->id = $id;
        if (!$this->Product->exists()) {
            throw new NotFoundException(__('Invalid Product'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Product->delete()) {
            $this->Flash->success(__('The product has been deleted.'));
        } else {
            $this->Flash->error(__('The product could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }
    private function upload($img,$img_name){
        $file = new File($img);
        $file_name = $img_name;
        if($file->copy(APP.'/webroot/images/products/'.$file_name)){
            return TRUE;
        } else {
            return FALSE;
        }
        
    }
    public function edit($id = NULL) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $length = 4;
                $rand1 = substr( str_shuffle( $characters ), 0, $length );
                $rand2 = substr( str_shuffle( $characters ), 0, $length );
            $rand = $rand1.'-'.$rand2;
        
        if (!$id) {
            throw new NotFoundException(__('Invalid Product'));
        }

        $product = $this->Product->findById($id);
        if (!$product) {
            throw new NotFoundException(__('Invalid Product'));
        }
        $this->loadModel('Category');
        $category = $this->Category->find('list');
        $this->set('category', $category);
        if ($this->request->is(array('post', 'put'))) {
            //xet xem co thay doi anh khong
            if (!empty($this->request->data['Product']['path_img1_new'])) {
                $img1 = $this->request->data['Product']['path_img1_new']['tmp_name'];
                $img_name1 = $rand.'-'.$this->request->data['Product']['path_img1_new']['name'];
                if($this->upload($img1,$img_name1)){
                    $file = new File(APP.'/webroot/images/products/'.$this->request->data['Product']['path_img1']);
                    $file->delete();
                    $this->request->data['Product']['path_img1'] = $img_name1;
                }
            } else {
                unset($this->request->data['Product']['path_img1']);
            }

            if (!empty($this->request->data['Product']['path_img2_new'])) {
                $img2 = $this->request->data['Product']['path_img2_new']['tmp_name'];
                $img_name2 = $rand.'-'.$this->request->data['Product']['path_img2_new']['name'];
                if($this->upload($img2,$img_name2)){
                    $file = new File(APP.'/webroot/images/products/'.$this->request->data['Product']['path_img2']);
                    $file->delete();
                    $this->request->data['Product']['path_img2'] = $img_name2;
                }
            } else {
                unset($this->request->data['Product']['path_img2']);
            }

            if (!empty($this->request->data['Product']['path_img3_new'])) {
                $img3 = $this->request->data['Product']['path_img3_new']['tmp_name'];
                $img_name3 = $rand.'-'.$this->request->data['Product']['path_img3_new']['name'];
                if($this->upload($img3,$img_name3)){
                    $file = new File(APP.'/webroot/images/products/'.$this->request->data['Product']['path_img3']);
                    $file->delete();
                    $this->request->data['Product']['path_img3'] = $img_name3;
                }
            } else {
                unset($this->request->data['Product']['path_img3']);
            }
            
            if (!empty($this->request->data['Product']['path_img4_new'])) {
                $img4 = $this->request->data['Product']['path_img4_new']['tmp_name'];
                $img_name4 = $rand.'-'.$this->request->data['Product']['path_img4_new']['name'];
                if($this->upload($img4,$img_name4)){
                    $file = new File(APP.'/webroot/images/products/'.$this->request->data['Product']['path_img4']);
                    $file->delete();
                    $this->request->data['Product']['path_img4'] = $img_name4;
                }
            } else {
                unset($this->request->data['Product']['path_img4']);
            }


            $this->Product->id = $id;

            if ($this->Product->save($this->request->data)) {
                $this->Flash->success(__('The Product has been updated.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to update The Product.'));
        }

        if (!$this->request->data) {
            $this->request->data = $product;
        }
        //$products = $this->Product->findById($id);
        $this->set('product', $product);
    }

    public function view($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid product'));
        }

        $product = $this->Product->findById($id);
        if (!$product) {
            throw new NotFoundException(__('Invalid product'));
        }
        $this->set('product', $product);
    }

}
