<?php

App::uses('Admin.AdminAppController', 'Controller');

class ReviewsController extends AdminAppController {

    public $components = array('Paginator', 'Flash');

    public function index() {
        $this->loadModel('Review');
        $this->paginate = array('limit' => 10);
        $reviews = $this->paginate('Review');        
        
        // Search
        if ($this->request->is('post')) {
            $keyword = $this->request->data['Review']['keyword'];
            $reviews = $this->Review->find('all', array(
                'conditions' => array(
                    'OR' => array('review like' => '%' . $keyword . '%',
                                   'Product.name like' => '%' . $keyword . '%'))
            ));
        }
        
        $this->set('reviews', $reviews);
    }

    public function view($id = null) {
        $this->loadModel('Review');
        if (!$id) {
            throw new NotFoundException(__('Invalid Review'));
        }

        $reviews = $this->Review->findById($id);
        if (!$reviews) {
            throw new NotFoundException(__('Invalid Review'));
        }
        $this->set('reviews', $reviews);
    }

    public function add() {
//          function add() {
//        if ($this->request->is('post')) {
//            $this->Review->set($this->request->data);
//            if ($this->Review->validates()) {
//                $this->Review->create();
////                pr($this->request->data);
////                die();
//                if ($this->Review->save($this->request->data)) {
//                    $this->Flash->success(__('Gửi bình luận thành công!'));
//                } else {
//                    $this->Flash->error(__('Please, try again!'));
//                }
//            }
//        } else {
//            $comment_errors = $this->Review->validationErrors;
//            $this->Session->write('comment_errors', $comment_errors);
//        }
//        $this->redirect($this->referer());
//    }
        
        $this->loadModel('Review');
        $this->loadModel('User');
        $users = $this->User->find('list', array('fields' => array('id', 'username')));
        $this->set('users', $users);

        $this->loadModel('Product');
        $product = $this->Product->find('list');
        $this->set('product', $product);

        if ($this->request->is('post')) {
            $this->Review->create();
            if ($this->Review->save($this->request->data)) {
                $this->Session->setFlash(__('The Review has been saved.'));
                return $this->redirect(array('action' => 'index '));
            } else {
                $this->Flash->error(__('The Review could not be saved. Please, try again.'));
            }
        }
    }

    public function delete($id = null) {
        $this->Review->id = $id;
        if (!$this->Review->exists()) {
            throw new NotFoundException(__('Invalid Review'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Review->delete()) {
            $this->Flash->success(__('The Review has been deleted.'));
        } else {
            $this->Flash->error(__('The Review could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function edit($id = null) {
        $this->loadModel('Review');
        if (!$id) {
            throw new NotFoundException(__('Invalid Review'));
        }

        $reviews = $this->Review->findById($id);
        if (!$reviews) {
            throw new NotFoundException(__('Invalid Review'));
        }
        $this->loadModel('User');
        $users = $this->User->find('list', array('fields' => array('id', 'username')));
        $this->set('users', $users);

        $this->loadModel('Product');
        $product = $this->Product->find('list');
        $this->set('product', $product);

        if ($this->request->is(array('post', 'put'))) {
            $this->Review->id = $id;
            if ($this->Review->save($this->request->data)) {
                $this->Flash->success(__('The Review has been updated.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to update The Review.'));
        }

        if (!$this->request->data) {
            $this->request->data = $reviews;
        }
        $this->set('reviews', $reviews);
    }

}
