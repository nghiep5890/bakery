<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Admin.AdminAppController', 'Controller');
App::uses('File', 'Utility');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class UsersController extends AdminAppController {

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array('Users');

    /**
     * Displays a view
     *
     * @return void
     * @throws NotFoundException When the view file could not be found
     * 	or MissingViewException in debug mode.
     */
    public function index() {
        $this->paginate = array('limit' => 10);
        $user = $this->paginate('Users');

        // Search
        if ($this->request->is('post')) {
            $keyword = $this->request->data['Users']['keyword'];
            $user = $this->Users->find('all', array(
                'fields' => array('id','username','avatar', 'email','created'),
                'conditions' => array(
                    'OR' => array('username like' => '%' . $keyword . '%',
                        'email like' => '%' . $keyword . '%'))
                ));
        }
            $this->set('users', $user);

    }


    public function logout() {
        $this->Auth->logout();
        return $this->redirect("/");
    }

    public function add() {
        if ($this->request->is('post')) {
            $img = $this->request->data['Users']['avatar']['tmp_name'];
            $img_name = $this->request->data['Users']['avatar']['name'];
            $result = $this->upload($img,$img_name);
            if($result){
                $this->request->data['Users']['avatar'] = $img_name;
                $this->Users->create();
                if ($this->Users->save($this->request->data)) {
                    $this->Session->setFlash(__('The User has been saved.'));
                    return $this->redirect(array('action' => 'index '));
                } else {
                    $this->Flash->error(__('The User could not be saved. Please, try again.'));
                }
            }
        }
        
    }

    public function view($id = null) {
//        if (!w NotFoundException(__('Invalid product'));
        
        
        $user = $this->Users->findById($id);
            $this->set('users', $user);
    }
//    private function upload($img,$img_name){
//        $file = new File($this->request->data['Users']['avatar_new']['tmp_name']);
//        $file_name = $this->request->data['Users']['avatar_new']['name'];
//        $file->copy(APP.'/webroot/images/avatar/'.$file_name);
//        if($file->copy(APP.'/plugin/admin/webroot/images//avatar/'.$file_name)){
//            return TRUE;
//        } else {
//            return FALSE;
//        }
//    }
    
    private function upload($img,$img_name){
        $file = new File($img);
        $file_name = $img_name;
        if($file->copy(APP.'/webroot/images/avatar/'.$file_name)){
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function edit($id = null) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $length = 4;
                $rand1 = substr( str_shuffle( $characters ), 0, $length );
                $rand2 = substr( str_shuffle( $characters ), 0, $length );
            $rand = $rand1.'-'.$rand2;
        
        if (!$id) {
            throw new NotFoundException(__('Invalid User'));
        }

        $user = $this->Users->findById($id);
        if (!$user) {
            throw new NotFoundException(__('Invalid User'));
        }
        
        if ($this->request->is(array('post', 'put'))) {
            if (!empty($this->request->data['Users']['avatar_new'])) {
                $img = $this->request->data['Users']['avatar_new']['tmp_name'];
                $img_name = $rand.'-'.$this->request->data['Users']['avatar_new']['name'];
            if($this->upload($img,$img_name)){
                $file = new File(APP.'/webroot/images/avatar/'.$this->request->data['Users']['avatar']);
                $file->delete();
                    $this->request->data['Users']['avatar'] = $img_name;
                    
                }
            } else {
                unset($this->request->data['Users']['avatar']);
            }
//            pr($doi);die;
            $this->Users->id = $id;
            
            if ($this->Users->save($this->request->data)) {
                $this->Flash->success(__('The User has been updated.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to update The User.'));
        }

        if (!$this->request->data) {
            $this->request->data = $user;
        }
        $this->set('users', $user);
    }
    public function delete($id = null) {
		$this->Users->id = $id;
		if (!$this->Users->exists()) {
			throw new NotFoundException(__('Invalid User'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Users->delete()) {
			$this->Flash->success(__('The user has been deleted.'));
		} else {
			$this->Flash->error(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
