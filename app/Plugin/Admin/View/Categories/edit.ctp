<legend><?php echo __('Edit Category'); ?></legend>
<div class="edit">
    <article class="col-sm-9">
        <div class="Users form">
            <?php
            echo $this->Form->create('Category',array('escape' => false,'type'=>'file'));
            echo $this->Form->input('name');
            echo $this->Form->input('image');
            echo $this->Html->image("../images/".$categories['Category']['image'],array('height'=>'50px'));
            echo $this->Form->input('image_new',array('type'=>'file','class'=>'input-upload'));
            echo $this->Form->input('info');
            echo $this->Form->input('id', array('type' => 'hidden'));
            echo $this->Form->end('Save');
            ?>
        </div>
    </article>
</div>
