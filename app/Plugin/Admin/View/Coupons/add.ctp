<div class="productadd">
    <?php echo $this->Form->create('Coupon');  ?>
		<fieldset>
			<legend><?php echo __('Add New Coupon'); ?></legend>
			<dl>
				<dt>Code:</dt><dd><?= $this->Form->input('code',array('label' =>false,'required' => 'required')); ?></dd>
				<dt>Percent:</dt><dd><?= $this->Form->input('percent',array('label' =>false,'min'=>1,'required' => 'required')); ?></dd>
				<dt>Description:</dt><dd><?= $this->Form->input('description',array('label' => false,'type' => 'textarea','class'=>'ckeditor')); ?></dd>
                                <dt>Time start:</dt><dd><?= $this->Form->input('time_start',array('label' =>false)); ?>
                                <dt>Time end:</dt><dd><?= $this->Form->input('time_end',array('label' =>false)); ?>
			</dl>
		</fieldset>
	<?php  echo $this->Form->button('Reset', array('type' => 'reset'));
                echo $this->Form->end(__('Save')); ?>
</div>