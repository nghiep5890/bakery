<legend><?php echo __('Edit Coupon'); ?></legend>
<div class="edit">
    <article class="col-sm-9">
        <div class="Users form">
            <?php
                echo $this->Form->create('Coupon');
                echo $this->Form->input('code');
                echo $this->Form->input('percent',array('min'=>1));
                echo $this->Form->input('description');
                echo $this->Form->input('time_start');
                echo $this->Form->input('time_end');
                echo $this->Form->end('Save');
            ?>
        </div>
    </article>
</div>