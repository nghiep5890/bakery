<div id="controls">
    <div class="add"  style="float: right">
        <div class="addButton">
            <?= $this->Html->link(
                    $this->Html->tag('i', '', array('class' => 'fa fa-plus fa-fw')).'Add',
                    array(
                        'controller' => 'coupons',
                        'action' => 'add'),
                    array('escape' => false)
            ); ?>
        </div>
    </div>
    <div class="col-md-4">
        <?=  $this->Form->create('Coupon', array('type' => 'post')); ?>
        <?=  $this->Form->input('keyword',array('class'=>'span3','label'=>false, 'placeholder'=>'Search...')); ?>
        <?=$this->Form->end(); ?>
    </div>
    
</div>
<table class="table">
<thead>
    <tr>   
        <th><?php echo $this->Paginator->sort('id');?></th>
        <th><?php echo $this->Paginator->sort('code'); ?></th>
        <th><?php echo $this->Paginator->sort('percent'); ?></th>
        <th><?php echo $this->Paginator->sort('time_start'); ?></th>
        <th><?php echo $this->Paginator->sort('time_end'); ?></th>
        <th>Action</th>
    </tr>
</thead>
<tbody>
    <?php if(empty($coupons)){?>
        <h4 style="color : red">No found coupons</h4>;
    <?php   } else{ 
                foreach ($coupons as $coupons){ ?>
    <tr>
        <td><?= $coupons['Coupon']['id']; ?></td>
        <td><?= $coupons['Coupon']['code']; ?></td>
        <td><?= $coupons['Coupon']['percent']; ?></td>
        <td><?= $coupons['Coupon']['time_start']; ?></td>
        <td><?= $coupons['Coupon']['time_end']; ?></td>
        <td class="actions">
                    <?php echo $this->Html->link(__('View'), array('action' => 'view', $coupons['Coupon']['id'])); ?>
                    <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $coupons['Coupon']['id'])); ?>
                    <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $coupons['Coupon']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $coupons['Coupon']['id']))); ?>
        </td>
    </tr>    
    <?php }} ?>
</tbody>
</table>
<?php
    echo $this->Paginator->prev('« Previous ', null, null, array('class' => 'disabled'));
    echo " | ".$this->Paginator->numbers()." | "; 
    echo $this->Paginator->next(' Next »', null, null, array('class' => 'disabled'));
    echo "&nbsp;&nbsp;&nbsp;";
    echo " Page ".$this->Paginator->counter();
?>