<div class="productadd">
    <?php echo $this->Form->create('Faq');  ?>
		<fieldset>
			<legend><?php echo __('Add New Faq'); ?></legend>
			<dl>
				<dt>Question:</dt><dd><?= $this->Form->input('question',array('label' =>false,'required' => 'required')); ?></dd>
				<dt>Answer:</dt><dd><?= $this->Form->input('answer',array('label' => false,'type' => 'textarea','class'=>'ckeditor','required' => 'required')); ?></dd>
                                <dt>Approve<br>(0 : not display; 1 : display;):</dt><dd><?= $this->Form->input('approve',array('label' => false,'options' => array('0','1'),'selected' => '1')); ?></dd>
			</dl>

		</fieldset>
	<?php  echo $this->Form->button('Reset', array('type' => 'reset'));
                echo $this->Form->end(__('Save')); ?>
</div>