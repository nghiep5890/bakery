<div class="productadd">
    <?php echo $this->Form->create('Group');  ?>
		<fieldset>
			<legend><?php echo __('Add New Group'); ?></legend>
			<dl>
				<dt>Name:</dt><dd><?= $this->Form->input('name',array('label' =>false,'required' => 'required')); ?></dd>
				<dt>Description:</dt><dd><?= $this->Form->input('description',array('label' => false,'type' => 'textarea','class'=>'ckeditor','required' => 'required')); ?></dd>
			</dl>

		</fieldset>
	<?php  echo $this->Form->button('Reset', array('type' => 'reset'));
                echo $this->Form->end(__('Save')); ?>
</div>