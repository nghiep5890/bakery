<div id="controls">
    <div class="add"  style="float: right">
        <div class="addButton">
            <?= $this->Html->link(
                    $this->Html->tag('i', '', array('class' => 'fa fa-plus fa-fw')).'Add',
                    array(
                        'controller' => 'groups',
                        'action' => 'add'),
                    array('escape' => false)
            ); ?>
        </div>
    </div>
    <div class="col-md-4">
        <?= $this->Form->create('Group', array('type' => 'post')); ?>
        <?= $this->Form->input('keyword',array('class'=>'span3','label'=>false, 'placeholder'=>'Search...')); ?>
        <?= $this->Form->end(); ?>
    </div>
    
</div>
<table class="table">
<thead>
    <tr>   
        <th><?php echo $this->Paginator->sort('id'); ?></th>
        <th><?php echo $this->Paginator->sort('name'); ?></th>
        <th><?php echo $this->Paginator->sort('description'); ?></th>
        <th><?php echo $this->Paginator->sort('created'); ?></th>
        <th>Actions</th>

    </tr>
</thead>
<tbody>
    <?php if(empty($groups)){?>
        <h4 style="color : red">No found groups</h4>;
    <?php   } else{ 
                foreach ($groups as $groups){ ?>
    <tr>
        <td><?= $groups['Group']['id']; ?></td>
        <td><?= $groups['Group']['name']; ?></td>
        <td><?= $groups['Group']['description']; ?></td>
        <td><?= $groups['Group']['created']; ?></td>
        <td class="actions">
            <?php echo $this->Html->link(__('View'), array('action' => 'view', $groups['Group']['id'])); ?>
            <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $groups['Group']['id'])); ?>
            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $groups['Group']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $groups['Group']['id']))); ?>
        </td>
    </tr>    
    <?php }} ?>
</tbody>
</table>