<div class="container content">
    <div class="productadd">
        <h3>Customer information</h3>
        <table class="table">
            <thead>
                <tr>   
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Add</th>
                </tr>
            </thead>
            <tbody>
                <tr>
				<?php $data = array(
				    'customer_info' => json_decode($orders['Order']['customer_info']),
                                    'order_info' => json_decode($orders['Order']['order_info']),
                                    'payment_info' => json_decode($orders['Order']['payment_info'])
                                    );
                if($orders['Order']['user_id']) {
                	$id = $orders['Order']['user_id']?>
                    <td><?php echo $this->Html->link($orders['User']['username'],array(
                                                    'controller' => 'users',
                                                    'action' => 'view',
                                                    $id => $id)
                                                    );?>
                    </td>
                    <td><?php echo $orders['User']['mobilephone']; ?></td>
                    <td><?php echo $orders['User']['address']; ?></td>
                <?php } else { ?>
                    <td><?php echo $data['customer_info']->name;?></td>
                    <td><?php echo $data['customer_info']->phone; ?></td>
                    <td><?php echo $data['customer_info']->adress; ?></td>
                <?php } ?>
                </tr>
            </tbody>
        </table>
        <h3>Information products has order</h3>
        <table class="table">
        	<thead class="pro">
        		<tr>
                	<td> <h4>T-Quantity : </h4></td>
                	<td> <h4><?php echo $data['payment_info']->quantity_." products" ; ?></h4> </td>
                </tr>
                <tr>
                	<td> <h4>T-Price :</h4> </td>
                	<td> <h4><?php echo $data['payment_info']->total."$"; ?> </h4></td>
                </tr>
        		<?php if(isset($data['payment_info']->coupon)) {?>
        		<tr>
                	<td> <h4>Coupon : </h4></td>
                	<td> <h4><?php echo $data['payment_info']->coupon; ?></h4> </td>
                </tr>
                <tr>
                	<td> <h4>Discount : </h4></td>
                	<td> <h4><?php echo $data['payment_info']->discount."%" ; ?></h4> </td>
                </tr>
                
                <tr>
                	<td> <h4>Pay : </h4></td>
                	<td> <h4><?php echo $data['payment_info']->pay."$" ; ?></h4> </td>
                </tr>
        		<?php } ?>
                <tr>   
                    <th>Name</th>
                    <th>Image</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($data['order_info'] as $data) {
            		 ?>
                <tr>
                    <td><?php echo $data->name;?></td>
                    <td><?php echo $this->Html->image("../images/products/".$data->image,array('height'=>'50px'));?></td>
                    <td><?php echo $data->prices."$";?></td>
                    <td><?php echo $data->quantity;?></td>
                    <td><?php echo ($data->prices * $data->quantity)."$";?></td>
                </tr>
                <?php } ?>
                
            </tbody>
        </table>
    </div>
</div> 