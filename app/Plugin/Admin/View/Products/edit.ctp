<legend><?php echo __('Edit Product'); ?></legend>
<div class="edit productedit">
<?php
    echo $this->Form->create('Product',array('type'=>'file'));
        echo '<b>Category :</b><br>'; echo $this->Form->input('category_id',array('label' => false,'options'=>$category));
        echo $this->Form->input('name');
        echo $this->Form->input('description');
        echo $this->Form->input('path_img1');
        echo $this->Html->image("../images/products/".$product['Product']['path_img1'],array('height'=>'50px'));
        echo $this->Form->input('path_img1_new',array('label' => false,'type'=>'file'));
        echo $this->Form->input('path_img2');
        echo $this->Html->image("../images/products/".$product['Product']['path_img2'],array('height'=>'50px'));
        echo $this->Form->input('path_img2_new',array('label' => false,'type'=>'file'));
        echo $this->Form->input('path_img3');
        echo $this->Html->image("../images/products/".$product['Product']['path_img3'],array('height'=>'50px'));
        echo $this->Form->input('path_img3_new',array('label' => false,'type'=>'file'));
        echo $this->Form->input('path_img4');
        echo $this->Html->image("../images/products/".$product['Product']['path_img4'],array('height'=>'50px'));
        echo $this->Form->input('path_img4_new',array('label' => false,'type'=>'file'));
        echo $this->Form->input('price', array('min'=>0));
        echo $this->Form->input('expiry', array('min'=>0));
        echo $this->Form->input('rating', array('min'=>1,'max'=>5));
        echo $this->Form->input('sale_off', array('min'=>0));
        echo $this->Form->input('sale_start');
        echo $this->Form->input('sale_end');
        echo $this->Form->input('new');
        echo $this->Form->input('recommended');
        echo $this->Form->input('cold-storage');
        echo $this->Form->input('available');
        echo $this->Form->input('references');
        echo $this->Form->input('published');

        echo $this->Form->input('id', array('type' => 'hidden'));
    echo $this->Form->end('Save');
    ?>
</div>
