<div class="container content">
		<div class="productadd">
			<dl>
                            <dt><h3><?php echo __('Name:'); ?></h3></dt>
				<dd>
                                    <h3><b><?php echo $product['Product']['name']; ?></b></h3>
					&nbsp;
				</dd>
                                <dt><?php echo __('Description :'); ?></dt>
				<dd>
					<?php echo $product['Product']['description']; ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Img 1 :'); ?></dt>
				<dd>
					<?php echo $this->Html->image("../images/products/".$product['Product']['path_img1'],array('height'=>'50px')); ?>
					&nbsp;
				</dd>
                                <dt><?php echo __('Img 2 :'); ?></dt>
				<dd>
					<?php echo $this->Html->image("../images/products/".$product['Product']['path_img2'],array('height'=>'50px')); ?>
					&nbsp;
				</dd>
                                <dt><?php echo __('Img 3 :'); ?></dt>
				<dd>
					<?php echo $this->Html->image("../images/products/".$product['Product']['path_img3'],array('height'=>'50px')); ?>
					&nbsp;
				</dd>
                                <dt><?php echo __('Img 4 :'); ?></dt>
				<dd>
					<?php echo $this->Html->image("../images/products/".$product['Product']['path_img4'],array('height'=>'50px')); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Price :'); ?></dt>
				<dd>
					<?php echo ($product['Product']['price']); ?>
					&nbsp;
				</dd>
                                <dt><?php echo __('Expiry :'); ?></dt>
				<dd>
					<?php echo ($product['Product']['expiry']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Sale_off :'); ?></dt>
				<dd>
					<?php echo ($product['Product']['sale_off']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Sale_start :'); ?></dt>
				<dd>
					<?php echo ($product['Product']['sale_start']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Sale_end :'); ?></dt>
				<dd>
					<?php echo ($product['Product']['sale_end']); ?>
					&nbsp;
				</dd>
                                <dt><?php echo __('Rating :'); ?></dt>
				<dd>
					<?php echo ($product['Product']['rating']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Available :'); ?></dt>
				<dd>
					<?php echo ($product['Product']['available']); ?>
					&nbsp;
				</dd>
                                <dt><?php echo __('References :'); ?></dt>
				<dd>
					<?php echo ($product['Product']['references']); ?>
					&nbsp;
				</dd>
                                <dd ><a href="/admin/products/edit/<?php echo $product['Product']['id']; ?>" class="edit_view">Edit</a></dd>
			</dl>
		</div>
</div>

