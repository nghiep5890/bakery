<div class="productadd">
	<?php echo $this->Form->create('Review');  ?>
		<fieldset>
			<legend><?php echo __('Add New Review'); ?></legend>
			<dl>
				<dt>User:</dt><dd><?= $this->Form->input('user_id',array('label' => false,'options'=>$users)); ?></dd>
				<dt>Product:</dt><dd><?= $this->Form->input('product_id',array('label' => false,'options'=>$product)); ?></dd>
				<dt>Review:</dt><dd><?= $this->Form->input('review',array('label' =>false)); ?></dd>
				<dt>Published:<br>(0 : not display; 1 : display;)</dt><dd><?= $this->Form->input('approve',array('label' => false,'options' => array('0','1'),'selected' => '1')); ?></dd>
			</dl>
		</fieldset>
	<?php  echo $this->Form->button('Reset', array('type' => 'reset'));
                echo $this->Form->end(__('Save')); ?>
</div>
