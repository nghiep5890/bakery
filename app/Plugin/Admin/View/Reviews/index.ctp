<div id="controls">
    <div class="add"  style="float: right">
        <div class="addButton">
            <?= $this->Html->link(
                    $this->Html->tag('i', '', array('class' => 'fa fa-plus fa-fw')).'Add',
                    array(
                        'controller' => 'reviews',
                        'action' => 'add'),
                    array('escape' => false)
            ); ?>
        </div>
    </div>
    <div class="col-md-4">
        <?=  $this->Form->create('Review', array('type' => 'post')); ?>
        <?=  $this->Form->input('keyword',array('class'=>'span3','label'=>false, 'placeholder'=>'Search...')); ?>
        <?=$this->Form->end(); ?>
    </div>
    
</div>
<table class="table">
<thead>
    <tr>
        <th><?php echo $this->Paginator->sort('id');?></th>
        <th><?php echo $this->Paginator->sort('Product.name','Product');?></th>
        <th><?php echo $this->Paginator->sort('User.name','Username');?></th>
        <th style="width: 50%;"><?php echo $this->Paginator->sort('review','Review');?></th>
        <th>Actions</th>
        <th><?php echo $this->Paginator->sort('approve');?></th>
    </tr>
</thead>
<tbody>
    <?php if(empty($reviews)){?>
        <h4 style="color : red">No found reviews</h4>;
    <?php   } else{ 
                foreach ($reviews as $reviews){ ?>
        <tr>
            <td><?= $reviews['Review']['id']; ?></td>
            <td><?= $reviews['Product']['name']; ?></td>
            <td><?= $reviews['User']['username']; ?></td>
            <td><?= $reviews['Review']['review']; ?></td>
            <td class="actions">
                        <?php echo $this->Html->link(__('View'), array('action' => 'view', $reviews['Review']['id'])); ?>
                        <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $reviews['Review']['id'])); ?>
                        <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $reviews['Review']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $reviews['Review']['id']))); ?>
            </td>
            <td><?php if( $reviews['Review']['approve']==1){ ?>
                        <i class="fa fa-check fa-fw" style="color : green"></i>
                <?php  ;    } else {?>
                        <i class="fa fa-close fa-fw" style="color : red"></i>
                <?php  ; }?>
            </td>
        </tr>    
    <?php }
    } ?>
</tbody>
</table>
<?php
    echo $this->Paginator->prev('« Previous ', null, null, array('class' => 'disabled')); 
    echo " | ".$this->Paginator->numbers()." | "; 
    echo $this->Paginator->next(' Next »', null, null, array('class' => 'disabled'));
    echo "&nbsp;&nbsp;&nbsp;"; 
    echo " Page ".$this->Paginator->counter();
?>