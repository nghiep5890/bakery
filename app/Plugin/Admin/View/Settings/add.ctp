<div class="productadd">
	<?php echo $this->Form->create('Setting');  ?>
		<fieldset>
			<legend><?php echo __('Add New Setting'); ?></legend>
			<dl>
				<dt>Name:</dt><dd><?= $this->Form->input('name',array('label' =>false,'required' => 'required')); ?></dd>
				<dt>Value:</dt><dd><?= $this->Form->input('value',array('label' => false,'type' => 'textarea','class'=>'ckeditor','required' => 'required')); ?></dd>
			</dl>
		</fieldset>
	<?php  echo $this->Form->button('Reset', array('type' => 'reset'));
                echo $this->Form->end(__('Save')); ?>
</div>