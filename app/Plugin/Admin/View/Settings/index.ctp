<div id="controls">
    <div class="add"  style="float: right">
        <div class="addButton">
            <?= $this->Html->link(
                    $this->Html->tag('i', '', array('class' => 'fa fa-plus fa-fw')).'Add',
                    array(
                        'controller' => 'settings',
                        'action' => 'add'),
                    array('escape' => false)
            ); ?>
        </div>
    </div>
    <div class="col-md-4">
        <?=  $this->Form->create('Setting', array('type' => 'post')); ?>
        <?=  $this->Form->input('keyword',array('class'=>'span3','label'=>false, 'placeholder'=>'Search...')); ?>
        <?=$this->Form->end(); ?>
    </div>
    
</div>
<table class="table">
<thead>
    <tr>   
        <th><?php echo $this->Paginator->sort('id');?></th>
        <th><?php echo $this->Paginator->sort('name'); ?></th>
        <th><?php echo $this->Paginator->sort('value'); ?></th>
        <th>Action</th>
        <th><?php echo $this->Paginator->sort('updated'); ?></th>
    </tr>
</thead>
<tbody>
    <?php if(empty($settings)){?>
        <h4 style="color : red">No found settings</h4>;
    <?php      } else{ 
                foreach ($settings as $settings){ ?>
    <tr>
        <td><?= $settings['Setting']['id']; ?></td>
        <td><?= $settings['Setting']['name']; ?></td>
        <td  class="value"><?= $settings['Setting']['value']; ?></td>
        <td class="actions">
                    <!-- <?php echo $this->Html->link(__('View'), array('action' => 'view', $settings['Setting']['id'])); ?> -->
                    <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $settings['Setting']['id'])); ?>
                    <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $settings['Setting']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $settings['Setting']['id']))); ?>
                   
        </td>
        <td><?= $settings['Setting']['updated']; ?></td>
    </tr>    
    <?php }} ?>
</tbody>
</table>
<?php
    echo $this->Paginator->prev('« Previous ', null, null, array('class' => 'disabled')); 
    echo " | ".$this->Paginator->numbers()." | "; 
    echo $this->Paginator->next(' Next »', null, null, array('class' => 'disabled')); 
    echo "&nbsp;&nbsp;&nbsp;";
    echo " Page ".$this->Paginator->counter(); 
?>