<div id="controls">
    <div class="add"  style="float: right">
        <div class="addButton">
            <?= $this->Html->link(
                    $this->Html->tag('i', '', array('class' => 'fa fa-plus fa-fw')).'Add',
                    array(
                        'controller' => 'users',
                        'action' => 'add'),
                    array('escape' => false)
            ); ?>
        </div>
    </div>
    <div class="col-md-4">
        <?=  $this->Form->create('Users', array('type' => 'post')); ?>
        <?=  $this->Form->input('keyword',array('class'=>'span3','label'=>false, 'placeholder'=>'Search...')); ?>
        <?=$this->Form->end(); ?>
    </div>
    
</div>
<table class="table">
<thead>
    <tr>
        <th><?php echo $this->Paginator->sort('id');?></th>
        <th><?php echo $this->Paginator->sort('username','Name');?></th>
        <th><?php echo $this->Paginator->sort('avatar','Avatar');?></th>
        <th><?php echo $this->Paginator->sort('email'); ?></th>
        <th><?php echo $this->Paginator->sort('created'); ?></th>
        <th>Actions</th>
    </tr>
</thead>
<tbody>
    <?php if(empty($users)){?>
        <h4 style="color : red">No found users</h4>;
    <?php   } else{ 
                foreach ($users as $users){ ?>
    <tr>
        <td><?= $users['Users']['id']; ?></td>
        <td><?= $users['Users']['username']; ?></td>
        <td><?php if (!empty($users['Users']['avatar'])) { 
            echo $this->Html->image("../images/avatar/".$users['Users']['avatar'],array('height'=>'50px'));
            }else{
            echo $this->Html->image("/images/avatar/no-image-thumb.png", array('style' => ('height:50px;')));
            } ?></td>
        <td><?= $users['Users']['email']; ?></td>
        <td><?= $users['Users']['created']; ?></td>
        <td class="actions">
                    <?php echo $this->Html->link(__('View'), array('action' => 'view', $users['Users']['id'])); ?>
                    <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $users['Users']['id'])); ?>
                    <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $users['Users']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $users['Users']['id']))); ?>
        </td>
    </tr>    
    <?php }}?>
</tbody>
</table>
<?php
    echo $this->Paginator->prev('« Previous ', null, null, array('class' => 'disabled')); 
    echo " | ".$this->Paginator->numbers()." | "; 
    echo $this->Paginator->next(' Next »', null, null, array('class' => 'disabled')); 
    echo "&nbsp;&nbsp;&nbsp;";
    echo " Page ".$this->Paginator->counter();
?>






