<div class="container content">
		<div class="productadd">
			<dl>
                            <dt><h3><?php echo __('Full Name'); ?></h3></dt>
				<dd>
                                    <h3><b><?php echo ($users['Users']['last_name'])." ".$users['Users']['first_name']; ?></b></h3>
					&nbsp;
				</dd>
				<dt><?php echo __('Avatar'); ?></dt>
				<dd>
					<?php echo $this->Html->image("../images/avatar/".$users['Users']['avatar']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Email'); ?></dt>
				<dd>
					<?php echo ($users['Users']['email']); ?>
					&nbsp;
				</dd>
                                <dt><?php echo __('Birthday'); ?></dt>
				<dd>
					<?php echo ($users['Users']['birth']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Homephone'); ?></dt>
				<dd>
					<?php echo ($users['Users']['homephone']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Mobilephone'); ?></dt>
				<dd>
					<?php echo ($users['Users']['mobilephone']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Adddress'); ?></dt>
				<dd>
					<?php echo ($users['Users']['address']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Created'); ?></dt>
				<dd>
					<?php echo ($users['Users']['created']); ?>
					&nbsp;
				</dd>
                                <dd ><a href="/admin/users/edit/<?php echo $users['Users']['id']; ?>" class="edit_view">Edit</a></dd>
			</dl>
		</div>
</div>