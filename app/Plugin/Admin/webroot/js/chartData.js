
var visitorsData = {
    title:{
            text: "Visitors - per month",
            },
            axisX: {
              valueFormatString: "MMMM",
              interval: 1,
              intervalType: "month"
            },
            axisY:{
              includeZero: false

            },
            data: [
            {
              type: "line",
              color: "rgba(170,170,170,1)",

              dataPoints: [
              { x: new Date(2016, 00, 1), y: 4510 },
              { x: new Date(2016, 01, 1), y: 4214},
              { x: new Date(2016, 02, 1), y: 1520},
              { x: new Date(2016, 03, 1), y: 2460 },
              { x: new Date(2016, 04, 1), y: 4530 },
              { x: new Date(2016, 05, 1), y: 5200 },
              { x: new Date(2016, 06, 1), y: 3480 },
              { x: new Date(2016, 07, 1), y: 2380 },
              ]
            }
            ]
};
var ordersData = {
    title:{
            text: "Orers - per month",
            },
            axisX: {
              valueFormatString: "MMMM",
              interval: 1,
              intervalType: "month"
            },
            axisY:{
              includeZero: false

            },
            data: [
            {
              type: "line",
              color: "rgba(0,170,170,1)",

              dataPoints: [
              { x: new Date(2016, 00, 1), y: 450 },
              { x: new Date(2016, 01, 1), y: 414 },
              { x: new Date(2016, 02, 1), y: 520},
              { x: new Date(2016, 03, 1), y: 460 },
              { x: new Date(2016, 04, 1), y: 450 },
              { x: new Date(2016, 05, 1), y: 500 },
              { x: new Date(2016, 06, 1), y: 480 },
              { x: new Date(2016, 07, 1), y: 480 },
              ]
            }
            ]
};
var salesData = {

            title:{
            text: "Sales - per month",
            },
            axisX: {
              valueFormatString: "MMMM",
              interval: 1,
              intervalType: "month"
            },
            axisY:{
              includeZero: false

            },
            data: [
            {
              type: "line",
              color: "rgba(233,78,2,1)",

              dataPoints: [
              { x: new Date(2016, 00, 1), y: 3150 },
              { x: new Date(2016, 01, 1), y: 1414 },
              { x: new Date(2016, 02, 1), y: 4520 },
              { x: new Date(2016, 03, 1), y: 2260 },
              { x: new Date(2016, 04, 1), y: 4501 },
              { x: new Date(2016, 05, 1), y: 6300 },
              { x: new Date(2016, 06, 1), y: 3830 },
              { x: new Date(2016, 07, 1), y: 9820 },
              ]
            }
            ]
          };