<footer>
    	<div id="footer" class="container">
	    	<div class="footer footer-contact">
	    		<h3>contact</h3>
	    		<ul>
	    			<li><i class="fa fa-map-marker fa-fw" style="color:#fff;" aria-hidden="true"></i>
	    				<span><?php echo $addr['Setting']['value']; ?></span></li>
	    			<li><i class="fa fa-phone fa-fw" style="color:#fff;" aria-hidden="true"></i>
	    				<span><?php echo $phone1['Setting']['value']; ?><br><?php echo $phone2['Setting']['value']; ?></li>
	    			<li><i class="fa fa-envelope fa-fw" style="color:#fff;" aria-hidden="true"></i>
	    				<span><?php echo $email['Setting']['value']; ?></li>
	    		</ul>
	    	</div>
	    	<div class="footer footer-account">
	    		<h3>account</h3>
	    		<ul>
	    			<li><a href="#">Order History</a></li>
	    			<li><a href="#">Login</a></li>
	    			<li><p><?php echo $this->Html->link('Register', array('controller' => 'users', 'action' => 'register')); ?></p></li>
	    			<li><a href="#">My Wishlist</a></li>
	    			<li><p><?php echo $this->Html->link('Check Out', array('controller' => 'products', 'action' => 'check_out')); ?></p></li>
	    		</ul>
	    	</div>
	    	<div class="footer footer-support">
	    		<h3>support</h3>
	    		<ul>
	    			<li><p><?php echo $this->Html->link('Contact us', array('controller' => 'pages', 'action' => 'shopping_guide#map')); ?></p></li>
	    			<li><a href="#">Terms &amp; Conditions</a></li>
	    			<li><p><?php echo $this->Html->link('Shipping policy', array('controller' => 'pages', 'action' => 'shopping_guide#return')); ?></p></li>
	    			<li><p><?php echo $this->Html->link('Site map', array('controller' => 'pages', 'action' => 'shopping_guide#map')); ?></p></li>
	    			<li><p><?php echo $this->Html->link('F.A.Qs', array('controller' => 'pages', 'action' => 'shopping_guide#faq')); ?></p></li>
	    		</ul>
	    	</div>
	    	<div class="footer footer-gallery">
	    		<h3>gallery</h3>
                    <?php if($product_footer){
                        foreach ($product_footer as $key => $product_footer){ ?>
    					<div class="ft-gallery <?php echo 'footer-gallery-'.$key ; ?>"><a href="<?php echo $this->Html->url(array('controller' => 'products', 'action' => 'detail', '?' => array('id' => $product_footer['Product']['id']), )); ?>">
                        <?php echo $this->Html->image('../images/products/'.$product_footer['Product']['path_img1']); ?></a>
                        </div>
                	 <?php }}?>
	    	</div>
    	</div>
    	<div id="coppyright">
    		<span>coppyright &copy; 2016 <?php echo ($site_name['Setting']['value']); ?> - all rights reserved</span>
    	</div>
    </footer>