    <div class="overlay" style="display:none;">
        <div class="login-wrapper">
            <div class="login-content">
                <a class="close">x</a>
                <h3>Sign in</h3>
                
                <?= $this->Form->create('User',
                                    array(
                                        'controller' => 'users',
                                        'action' => 'login'),
                                    array(
                                        'type' =>'POST'
                                    )); ?>
                    <label for="username">
                        Username:
                        <?= $this->Form->input('username',
                            array(
                                'placeholder'=>'Username must be between 5 and 32 characters',
                                'pattern'=>"^[a-z0-9]{5,32}$",
                                'required'=>'required',
                                'autofocus' => 'autofocus',
                                'label'=>false
                            )
                        ) ?>
                    </label>
                    <label for="password">
                        Password:
                        <?= $this->Form->input('password',
                            array(
                                'placeholder'=>'Password',
                                'required'=>'required',
                                'label'=>false
                        )); ?>
                    </label>
                        <input type="checkbox" name="data[User][remember_me]" value="S"> <?= __('Remember me')?>
                    <div class="form-group">
                        <?= $this->Html->link(__(
                            'Forgot your password?'),
                            array(
                                'controller' => 'users',
                                'action' => 'remember_password'));
                        ?>
                    
                        <?= $this->Html->link(
                            'Create a new account',
                            array(
                                'controller' => 'users',
                                'action' => 'register'));
                        ?>
                    </div>
                    <div class="btn-login">
                        <?= $this->Form->button('Sign in', array('type'=>'submit')); ?>
                    </div>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
<script type="text/javascript">
$(document).ready(function() {
    $(".clicklogin").click(function( event ) {
        event.preventDefault();
        $(".overlay").fadeToggle("fast");
    });
     
    $(".overlayLink").click(function(event) {
        event.preventDefault();
        var action = $(this).attr('data-action');
         
        $.get( "ajax/" + action, function( data ) {
            $( ".login-content" ).html( data );
        }); 
         
        $(".overlay").fadeToggle("fast");
    });
     
    $(".close").click(function() {
        $(".overlay").fadeToggle("fast");
    });
     
    $(document).keyup(function(e) {
        if(e.keyCode == 27 && $(".overlay").css("display") != "none" ) { 
            event.preventDefault();
            $(".overlay").fadeToggle("fast");
        }
    });
});
</script>