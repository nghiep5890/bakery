<nav>
    <div id="nav-menu" class="container">
        <div class="icon-menu">
            <a id="c-button--push-left" class="c-button" href="#"><i class="fa fa-navicon fa-fw"></i></a>
        </div>
        <ul>
            <li><?php echo $this->Html->link('Home', '/'); ?></li>
            <li><?php echo $this->Html->link('About Us', '/about_us'); ?></li>
            <li><?php echo $this->Html->link('Products', '/products'); ?></li>
            <li><?php echo $this->Html->link('Quality', '/quality'); ?></li>
            <li><?php echo $this->Html->link('Shopping Guide', '/shopping_guide'); ?></li>
            <li><?php echo $this->Html->link('contact', array('controller' => 'pages', 'action' => 'shopping_guide#map')); ?></li>
        </ul>
        <div class="icon-search">
            <i class="fa fa-search fa-fw"></i>
        </div>
    </div>
</nav>