<section id="content">        <!--        ///////////////         CONTENT         //////////////       -->
     <!--        ///////////////         SLIDER         //////////////       -->
    <div id="wowslider-container1">
    <div class="ws_images slider-img">
        <ul>
            <li><img src="data1/images/bakery_spikes_knives_boards_baking_75128_1920x1080.jpg" alt="bakery_spikes_knives_boards_baking_75128_1920x1080" title="bakery_spikes_knives_boards_baking_75128_1920x1080" id="wows1_0"/></li>
            <li><img src="data1/images/6842381bakerywallpaper.jpg" alt="6842381-bakery-wallpaper" title="6842381-bakery-wallpaper" id="wows1_1"/></li>
            <li><img src="data1/images/bakery_and_coffeewide.jpg" alt="bakery_and_coffee-wide" title="bakery_and_coffee-wide" id="wows1_2"/></li>
            <li><img src="data1/images/ti_xung.jpg" alt="tải xuống" title="tải xuống" id="wows1_3"/></li>
            <li><img src="data1/images/bakeryfoodwallpaper3.jpg" alt="bakery-food-wallpaper-3" title="bakery-food-wallpaper-3" id="wows1_4"/></li>
            <li><img src="data1/images/bakerywallpaperhd1.jpg" alt="bakerywallpaperhd1" title="bakery-wallpaper-hd-1" id="wows1_5"/></a></li>
            <li><img src="data1/images/soooogood.jpg" alt="soooo-good" title="soooo-good" id="wows1_6"/></li>
        </ul>
    </div>
    <div class="slider-info">
        <img src="images/slide_crown.png" />
        <h1>Welcome to us</h1>
        <h2>japannese bakery store</h2>
        <h4>Certificated by Gurus from begining to now</h4> 
        <?php echo $this->Html->link('Discover More <i class="fa fa-long-arrow-right fa-fw"></i>', array('controller' => 'products', 'action' => 'index'), array('escape' => false, 'class' => 'btn btn-fa')); ?>
    </div>
    <div class="ws_bullets">
        <div>
            <a href="#" title="bakery_spikes_knives_boards_baking_75128_1920x1080"><span><img src="data1/tooltips/bakery_spikes_knives_boards_baking_75128_1920x1080.jpg" alt="bakery_spikes_knives_boards_baking_75128_1920x1080"/>1</span></a>
            <a href="#" title="6842381-bakery-wallpaper"><span><img src="data1/tooltips/6842381bakerywallpaper.jpg" alt="6842381-bakery-wallpaper"/>2</span></a>
            <a href="#" title="bakery_and_coffee-wide"><span><img src="data1/tooltips/bakery_and_coffeewide.jpg" alt="bakery_and_coffee-wide"/>3</span></a>
            <a href="#" title="tải xuống"><span><img src="data1/tooltips/ti_xung.jpg" alt="tải xuống"/>4</span></a>
            <a href="#" title="bakery-food-wallpaper-3"><span><img src="data1/tooltips/bakeryfoodwallpaper3.jpg" alt="bakery-food-wallpaper-3"/>5</span></a>
            <a href="#" title="bakery-wallpaper-hd-1"><span><img src="data1/tooltips/bakerywallpaperhd1.jpg" alt="bakery-wallpaper-hd-1"/>6</span></a>
            <a href="#" title="soooo-good"><span><img src="data1/tooltips/soooogood.jpg" alt="soooo-good"/>7</span></a>
        </div>
    </div>
    
    </div>  
    <!--        ///////////////         CONTAINER         //////////////
-->
    <div class="container">
        <!--        ///////////////         CATEGORY         //////////////       -->
        <?= $this->element('aside'); ?>
        <!--        ///////////////         ARTICLE         //////////////       -->

        <article>
                <div class="category">
                    <div class="category-group">
                        <div class="category-img">
                            <img src="images/category_img1.png">	
                        </div>
                        <div class="category-info">
                            <h2>bread</h2>    			
                            <p>Lorem Khaled Ipsum is a major key to success. The key to more success is to get a message once a week, very important</p>
                            <button type="button">Shop now <i class="fa fa-long-arrow-right fa-fw"></i></button>
                        </div>
                    </div>
                    <div class="category-group">
                        <div class="category-img">
                            <img src="images/category_img2.png">	
                        </div>
                        <div class="category-info">
                            <h2>coffee &amp; tea</h2>    			
                            <p>Lorem Khaled Ipsum is a major key to success. The key to more success is to get a message once a week, very important</p>
                            <button type="button">Shop now <i class="fa fa-long-arrow-right fa-fw"></i></button>
                        </div>
                    </div>
                    <div class="category-group">
                        <div class="category-img">
                            <img src="images/category_img3.png">	
                        </div>
                        <div class="category-info">
                            <h2>bakery</h2>    			
                            <p>Lorem Khaled Ipsum is a major key to success. The key to more success is to get a message once a week, very important</p>
                            <button type="button">Shop now <i class="fa fa-long-arrow-right fa-fw"></i></button>
                        </div>
                    </div>
                </div>
            <!--        ///////////////         BANNER         //////////////       -->
                <script>
                        $(function () {
                          $("#slider2").responsiveSlides({
                            auto: true,
                            pager: false,
                            nav: false,
                            speed: 700,
                            namespace: "callbacks",
                            before: function () {
                              $('.events').append("<li>before event fired.</li>");
                            },
                            after: function () {
                              $('.events').append("<li>after event fired.</li>");
                            }
                          });
                        });
                </script>
                <div class="callbacks_container banner">
                    <ul class="rslides" id="slider2">
                        <?php foreach ($product_slide as $product_slide): ?>
                            <li>
                                <div class="testimonial-left banner-l">
                                    <?php echo $this->Html->image('/images/products/'.$product_slide['Product']['path_img1']); ?>
                                </div>
                                <div class="testimonial-right banner-r">
                                    <h2 class="contenth2"><?php echo $product_slide['Product']['name']; ?></h2>
                                    <p><?php echo $product_slide['Product']['description'];?></p>
                                    
                                    <a href="<?php echo $this->Html->url(array('controller' => 'products', 'action' => 'detail', '?' => array('id' => $product_slide['Product']['id']), )); ?>">shop now <i class="fa fa-long-arrow-right fa-fw"></i></a>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <!--        ///////////////         PRODUCTS         //////////////        -->
            <div class="products">
                <div class="products-title">
                    <h1 class="contenth1">Discover now</h1>
                    <h2 class="contenth2">our featured items</h2>
                    <img src="images/banner_line.png">
                </div>
                <div class="products-group">
                    <div class="products-group">
                <?php $i = 1; $a = 1;
                    foreach ($product as $key => $product): ?>
                    <?php if(($key+1)%3) { ?>
                    <div class="prod-itemsp item1 product_view_<?php echo $i; ?>">
                    <?php }else{ ?>
                    <div class="prod-itemsp item2 product_view_<?php echo $i; ?>">
                    <?php } ?>
                        <div class="prod-item">
                                        <?php $id = $product['Product']['id']; ?>
                                        <div class="col-md-5 prod-info">
                                        <div class="prod-img">
                                            <?php echo $this->Html->image('../images/products/'.$product['Product']['path_img1']); ?>
                                            <div class="ashow">
                                                <a style="cursor:pointer;" class="show" id-product = "<?php echo $product['Product']['id']; ?>" product-view = "detail_product_in_list_<?php echo $a; ?>" onclick="quickviewP(this);">quick view</a>
                                            </div>
                                        </div>
                                        <h4><?php echo $product['Product']['name']; ?></h4>
                                <?php echo $this->Html->image('../images/rate'.$product['Product']['rating'].'.png'); ?>
                                <p><?php echo $this->Text->truncate(($product['Product']['description']),80); ?></p>
                                <h2>&#36;
                                    <?php
                                    if(($product['Product']['sale_off'] > 0) && (date('Y-m-d',strtotime($product['Product']['sale_start'])) < date('Y-m-d')) && (date('Y-m-d',strtotime($product['Product']['sale_end'])) > date('Y-m-d'))){
                                        echo $product['Product']['price'];
                                    }
                                    else{ echo $product['Product']['price']*(100-$product['Product']['sale_off'])/100;
                                    }
                                    ?>
                                    <span>&#36;<?php echo $product['Product']['price']; ?></span>
                                </h2>
                                <div class="btn-order">
                                <form accept-charset="utf-8" class="btn-form-order" method="post" id="ProductIndexForm" action="/products/add_to_cart/<?php echo $product['Product']['id'];?>">
                                    <input type="text" name="quantity" value="1" class="input-quantity" style="display:inline" />
                                    <input type="submit" value="Order" class="btn btn-add-cart" />
                                </form>
                                </div>
                                </div>
                        </div>
                    </div>
                    <?php if($i%3 == 0): ?>
                        <div class="detail_product_in_list_<?php echo $a; ?>"></div>
                    <?php $a++; endif; ?>
                    <?php $i++;?>
                <?php endforeach; ?>
            </div>
        </article>
    </div>

    <script type="text/javascript">
    
    function quickviewP(thiss){
        $("#quickview").remove();
        var class_show_product_view = $( thiss ).attr( "product-view" );
        var id_product = $( thiss ).attr( "id-product" );
        $.ajax({
            url: "<?php echo Router::url(array('controller' => 'products', 'action' => 'quickviewP'));  ?>",
            method: "POST",
            data: { id_products : id_product },
            dataType: "html", 
            success: function(result){
                $("."+class_show_product_view).html(result);
            }
        });        
        $(document).click(function(){
            $("#quickview").hide();
        });
    }

    </script>

<div class="testimonials">
    <div class="container">
        <div id="t_pagers">
            <div class="pagers">
                <a class="pager"><img src="../images/testimonials_1.jpg" /></a>
                <a class="pager"><img src="../images/testimonials_2.jpg" /></a>
                <a class="pager"><img src="../images/testimonials_3.jpg" /></a>
            </div>
        </div>
        <div id="test_container">
            <div class="testimonial">
                <div class="testimonial_text">Don’t panic, when it gets crazy and rough, don’t panic, stay calm. Fan luv. The ladies always say Khaled you smell good, I use no cologne. Cocoa butter is the key. Learning is cool, but knowing is better, and I know the key to success.</div>
                <h3 class="testimonial_name">MONALISA</h3>
                <div class="testimonial_designation">CEO, I Don't Care Inc.</div>
            </div>
            <div class="testimonial">
                <div class="testimonial_text">Don’t panic, when it gets crazy and rough, don’t panic, stay calm. Fan luv. The ladies always say Khaled you smell good, I use no cologne. Cocoa butter is the key. Learning is cool, but knowing is better, and I know the key to success.</div>
                <h3 class="testimonial_name">Larry Page</h3>
                <div class="testimonial_designation">CEO, Google Inc.</div>
            </div>
            <div class="testimonial">
                <div class="testimonial_text">Don’t panic, when it gets crazy and rough, don’t panic, stay calm. Fan luv. The ladies always say Khaled you smell good, I use no cologne. Cocoa butter is the key. Learning is cool, but knowing is better, and I know the key to success.</div>
                <h3 class="testimonial_name">Bill Gates</h3>
                <div class="testimonial_designation">Founder, Microsoft</div>
            </div>
        </div>
    </div>
</div>
        <script type="text/javascript">
            jQuery(document).ready(function($){
                var w,mHeight,tests=$(".testimonials");
                w=tests.outerWidth();
                mHeight=0;
                tests.find(".testimonial").each(function(index){
                    $("#t_pagers").find(".pager:eq(0)").addClass("active"); //make the first pager active initially
                    if(index==0)
                        $(this).addClass("active"); //make the first slide active initially
                    if($(this).height()>mHeight)    //just finding the max height of the slides
                        mHeight=$(this).height();
                    var l=index*w*1.3;            //find the left position of each slide
                    $(this).css("left",l);          //set the left position
                    tests.find("#test_container").height(mHeight);  //make the height of the slider equal to the max height of the slides
                });
                $(".pager").on("click",function(e){ //clicking action for pagination
                    e.preventDefault();
                    next=$(this).index(".pager");
                    clearInterval(t_int);   //clicking stops the autoplay we will define later
                    moveIt(next);
                });
                var t_int=setInterval(function(){   //for autoplay
                    var i=$(".testimonial.active").index(".testimonial");
                    if(i==$(".testimonial").length-1)
                        next=0;
                    else
                        next=i+1;
                    moveIt(next);
                },4000);
            });
            function moveIt(next){  //the main sliding function
                var c=parseInt($(".testimonial.active").removeClass("active").css("left")); //current position
                var n=parseInt($(".testimonial").eq(next).addClass("active").css("left"));  //new position
                $(".testimonial").each(function(){  //shift each slide
                    if(n>c)
                        $(this).animate({'left':'-='+(n-c)+'px'});
                    else
                        $(this).animate({'left':'+='+Math.abs(n-c)+'px'});
                });
                $(".pager.active").removeClass("active");   //very basic
                $("#t_pagers").find(".pager").eq(next).addClass("active");  //very basic
            }
        </script>

    <!--        ///////////////         ABOUT US         //////////////        -->
    <div class="container about-us">
        <div class="about-us-img">
            <img src="images/aboutus_chef.jpg" alt="">
        </div>
        <div class="about-r">
            <div class="about-us-info">
                <h1 class="contenth1">Let us tell you</h1>
                <h2 class="contenth2">story About us</h2>
                <hr>
                <p class="p1">l</p>
                <p class="p2">ook at the sunset, life is amazing, life is beautiful, life is what you make it. In life there will be road blocks but we will over come it.you going do is have</p>
                <p class="p3">is a major key to success. Don’t ever play yourself. They will try to close the door on you, just open it. Surround yourself with angels, positive energy, beautiful people, beautiful souls, clean heart, angel. </p>
                <p class="p4">They will try to close the door on you, just open it. Major key, don’t fall for the trap, stay focused. It’s the ones closest to you that want to see you fail. Let me be clear, you have to make it through the jungle to make it to paradise, that’s the key, Lion! Look at the sunset, life is amazing, life is beautiful, life is what you make it. It’s important to use cocoa butter.</p>
            </div>
        </div>
    </div>
</section>