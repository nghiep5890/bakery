<section id="content">
 <!-- ///////////////////////////         BANNER         //////////////////////////      -->
    	<div class="top-banner">
    		<h2>Shopping guide</h2>
    		<div class="page-link">
	    		<h3>Home
	    		<i class="fa fa-caret-right fa-fw" aria-hidden="true"></i>
	    		<span style="color:#fff;"> Shopping Guide</span></h3>
    		</div>
    	</div>
    	<div class="container content">
        <!--        ///////////////         CATEGORY         //////////////       -->
        <?= $this->element('aside'); ?>
        <!--        ///////////////         ARTICLE         //////////////       -->
    		<article>
    			<div class=" guide openinghours">
    				<img class="bg-fa" src="../images/header_ellipse1.png">
    				<i class="fa fa-clock-o fa-fw"></i>
    				<h3>opening hours</h3>
    				<div class="onweek">
    					<p>Form <span>Monday </span>to <span>Friday</span><br><?php echo $open['Setting']['value']; ?> - <?php echo $close['Setting']['value']; ?></p>
    				</div>    				
    				<div class="lastweek">
    					<p><span>Saturday </span>and <span>Sunday</span><br><?php echo $open_weekend['Setting']['value']; ?> - <?php echo $close_weekend['Setting']['value']; ?></p>
    				</div>
    				<div class="holiday">
    					<p><span>Holiday </span>is closed</p>
    				</div>    				
    			</div>
    			<div class=" guide shoppingguide">
    				<img class="bg-fa" src="../images/header_ellipse1.png">
    				<i class="fa fa-shopping-bag fa-fw"></i>
    				<h3>shopping guide</h3>
                    <p><?php echo nl2br($shopping['Setting']['value']); ?></p>
				</div>
    			<div class=" guide payment"><a name="payment"></a>
    				<img class="bg-fa" src="../images/header_ellipse1.png">
    				<i class="fa fa-money fa-fw"></i>
    				<h3>payment acctepted</h3>
    				<p>You should never complain, complaining is a weak emotion, you got life</p>
    				<div class="list-bank">
    					<img src="../images/banklg_1.png" alt="bank">
    					<img src="../images/banklg_2.png" alt="bank">
    					<img src="../images/banklg_3.png" alt="bank">
    					<img src="../images/banklg_4.png" alt="bank">
    					<img src="../images/banklg_5.png" alt="bank">
    				</div>

				</div>
    			<div class=" guide shipping"><a name="delivery"></a>
    				<img class="bg-fa" src="../images/header_ellipse1.png">
    				<i class="fa fa-truck fa-fw"></i>
    				<h3>Delivery and Shipping Fees</h3>
    				<p><?php echo $deli['Setting']['value']; ?></p>
                           
				</div>
    			<div class=" guide return"><a name="return"></a>
    				<img class="bg-fa" src="../images/header_ellipse1.png">
    				<i class="fa fa-refresh fa-fw"></i>
    				<h3>good return policy</h3>
    				<p><?php echo nl2br($return['Setting']['value']); ?></p>
				</div>
				<div class="map"><a name="map"></a>
					<iframe style="pointer-events: none;" src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d35408.473578814665!2d-0.09458901850284582!3d51.49411885016824!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1svi!2s!4v1463988704559" width="870" height="500" frameborder="0" wheelscroll="none" style="border:0" allowfullscreen>
					</iframe>
                    <div class="mapid">
                        <i class="fa fa-map-marker fa-fw"></i><p><?php echo $addr['Setting']['value']; ?></p>
                        <i class="fa fa-phone fa-fw"></i><p><?php echo $phone1['Setting']['value']; ?><br><?php echo $phone2['Setting']['value']; ?></p>
                        <i class="fa fa-envelope fa-fw"></i><p><?php echo $email['Setting']['value']; ?></p>
                    </div>
                </div>
                    <script>
                        $(function() {
                            $('.map').click(function(e) {
                                $(this).find('iframe').css('pointer-events', 'auto');
                            }).mouseleave(function(e) {
                                $(this).find('iframe').css('pointer-events', 'none');
                            });
                        })
                    </script>
                <div class=" guide faq"><a name="faq"></a>
                    <?php foreach ($faqs as $faqs): ?>
        				<section class="faq-section"><a name="faqs"></a>
        				    <input type="checkbox">
        				    <label><?php echo $faqs['Faq']['question']; ?></label>           
        				    <p><?php echo $faqs['Faq']['answer']; ?></p>  
        				</section>
                    <?php endforeach; ?>
                </div>
    		</article>
    	</div>
