<!-- ///////////////////////////         CONTENT         //////////////////////////      -->
<section id="content">
    <div class="top-banner"><!-- ///////////////////////////         BANNER         //////////////////////////      -->
        <h2>Product</h2>
        <div class="page-link">
            <h3>Home
                <i class="fa fa-caret-right fa-fw" aria-hidden="true"></i>
                <span>Product</span></h3>
        </div>
    </div>
    <div class="container content">
        <!-- ///////////////////////////         MENU LEFT         //////////////////////////      -->
        <?= $this->element('aside'); ?>
        <!-- ///////////////////////////         ARTICLE         //////////////////////////      -->
        <article>
            <script>
                $(document).ready(function(){
                    $('ul li').click(function(){
                        $('li').removeClass("active");
                        $(this).addClass("active");
                    });
                });
            </script>
            <div class="pagination page-sort">
                <div class="filter-l col-lg-6">
                    <i class="fa fa-th fa-fw"></i>
                    <i class="fa fa-th-list fa-fw"></i>
                </div>
                <div class="filter-r col-lg-6">
                    <?php echo $this->Form->create('Product '); ?>
                    <?php echo $this->Form->input('show', array('options' => array($this->Html->link('9', array('controller'=>'products', 'action' => 'list10')),12,18,30), 'class' => 'first-select', 'escape' => false)); ?>
                    <?php echo $this->Form->input('sortby', array('options' => array('name','price','rating'), 'class' => 'last-select')); ?>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
            <div class="products-group">
                <?php $i = 1;
                $a = 1;
                foreach ($product as $key => $product): ?>
                <?php if (($key + 1) % 3) { ?>
                <div class="prod-itemsp item1 product_view_<?php echo $i; ?>">
                    <?php }else{ ?>
                    <div class="prod-itemsp item2 product_view_<?php echo $i; ?>">
                        <?php } ?>
                        <div class="prod-item">
                            <?php $id = $product['Product']['id']; ?>
                            <div class="col-md-5 prod-info">
                                <div class="prod-img">
                                    <?php echo $this->Html->image('../images/products/' . $product['Product']['path_img1']); ?>
                                    <div class="ashow">
                                        <a style="cursor:pointer;" class="show"
                                           id-product="<?php echo $product['Product']['id']; ?>"
                                           product-view="detail_product_in_list_<?php echo $a; ?>"
                                           onclick="quickviewP(this);">quick view</a>
                                    </div>
                                    <?php if ($product['Product']['new'] == 1) { ?>
                                        <div class="new">
                                            <?php echo $this->Html->image('../images/prod_new.jpg'); ?>
                                        </div>
                                    <?php } ?>
                                    <?php if ($product['Product']['recommended'] == 1) { ?>
                                        <div class="star">
                                            <?php echo $this->Html->image('../images/prod-star.png'); ?>
                                        </div>
                                    <?php } ?>
                                    <?php if (($product['Product']['sale_off'] > 0) && (date('Y-m-d', strtotime($product['Product']['sale_start'])) < date('Y-m-d')) && (date('Y-m-d', strtotime($product['Product']['sale_end'])) > date('Y-m-d'))) {
                                        ?>
                                        <div class="saleoff">
                                            <span
                                                style="color: white;font-family: Montserrat-Regular;font-size: 12px;"><?php echo '-' . $product['Product']['sale_off'] . '%'; ?></span>
                                        </div>
                                    <?php } ?>
                                </div>
                                <h4><?php echo $product['Product']['name']; ?></h4>
                                <?php echo $this->Html->image('../images/rate' . $product['Product']['rating'] . '.png'); ?>
                                <p><?php echo $this->Text->truncate(($product['Product']['description']), 80); ?></p>
                                <h2>&#36;
                                    <?php
                                    if (($product['Product']['sale_off'] > 0) && ((date('Y-m-d', strtotime($product['Product']['sale_start'])) < date('Y-m-d')) && (date('Y-m-d', strtotime($product['Product']['sale_end'])) > date('Y-m-d')))) {
                                        echo $product['Product']['price'] * (100 - $product['Product']['sale_off']) / 100;
                                    } else {
                                        echo $product['Product']['price'];
                                    }
                                    ?>
                                    <span>&#36;<?php echo $product['Product']['price']; ?></span>
                                </h2>
                                <div class="btn-order">
                                    <form accept-charset="utf-8" method="post" id="ProductIndexForm" action="/products/add_to_cart/<?php echo $product['Product']['id'];?>">
                                        <input type="text" name="quantity" value="1" style="display:inline" />
                                        <input type="submit" value="Order" class="btn btn-add-cart"/>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if ($i % 3 == 0): ?>
                        <div class="detail_product_in_list_<?php echo $a; ?>"></div>
                        <?php $a++; endif; ?>
                    <?php $i++; ?>
                    <?php endforeach; ?>

                </div>
                <div class="pagination">
                    <?php $ta2 = $this->Paginator->param('current') + ($this->Paginator->param('page') - 1) * LIMIT_PRODUCT_CATEGORY; ?>
                    <?php $ta1 = 1 + ($this->Paginator->param('page') - 1) * LIMIT_PRODUCT_CATEGORY; ?>
                    <span> <?php echo 'Showing ' . $ta1 . ' - ' . $ta2 . ' in ' . $this->Paginator->param('count') . ' products'; ?></span>
                    <div class="pr_paginator">
                        <?php echo $this->Paginator->numbers($options = array('separator' => false)) . '  '; ?>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>
<script type="text/javascript">

    function quickviewP(thiss) {
        $("#quickview").remove();
        var class_show_product_view = $(thiss).attr("product-view");
        var id_product = $(thiss).attr("id-product");
        $.ajax({
            url: "<?php echo Router::url(array('controller' => 'products', 'action' => 'quickviewP'));  ?>",
            method: "POST",
            data: {id_products: id_product},
            dataType: "html",
            success: function (result) {
                $("." + class_show_product_view).html(result);
            }
        });
        $(document).click(function () {
            $("#quickview").hide();
        });
    }

</script>