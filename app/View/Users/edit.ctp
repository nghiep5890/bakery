<div class="container content">
        <!--        ///////////////         MenuLeft         //////////////       -->
        <?= $this->element('aside'); ?>
        <!--        ///////////////         ARTICLE         //////////////       -->    
      <article class="col-sm-9">
          <div class="add-user">
          <?php echo $this->Form->create('User',array('type'=>'file')); 
                
                $users = $this->request->data['User']['avatar'];
          ?>
              
              <fieldset>
                  <legend><?php echo __('Edit User'); ?></legend>
             <?php
                  echo $this->Form->input('first_name');
                  echo $this->Form->input('last_name');
                  echo $this->Form->input('avatar');
                if(!empty($users)){
                  echo $this->Html->image("../images/avatar/".$users,array('height'=>'50px'));
                } else{
                  echo $this->Html->image("../images/avatar/avatar-user.png",array('height'=>'50px'));
                }?>
                  <label for="Avatar">Avatar</label>
                 <?php
                  echo $this->Form->input('avatar_new',array('type'=>'file', 'label' => false));
                  echo "<h3>Birth</h3>";
                  echo $this->Form->input('birth', array(  
                                        'type' => 'date',
                                        'label' => false,
                                        'dateFormat' => 'YMD', 
                                        'minYear' => date('Y') - 80, 
                                        'maxYear' => date('Y') - 16
                                        ));
                  echo $this->Form->input('homephone');
                  echo $this->Form->input('mobilephone');
                  echo $this->Form->input('address');
                  echo $this->Form->button('Submit', array('type' => 'submit'));
              ?>
              </fieldset>
          <?php echo $this->Form->end(); ?>
          </div>
      </article>
</div>

