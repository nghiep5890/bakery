
<section id="content">
 
    <div class="container content">
            <!--        ///////////////         CATEGORY         //////////////       -->
            <?= $this->element('aside'); ?>
            <!--        ///////////////         ARTICLE         //////////////       -->
    <article>
            <div class="add-user">
                    <?php echo $this->Form->create('User',array( 'url'=> array('controller' => 'users','action' => 'register'),'type'=>'file')); ?>
                        <fieldset>
                            <legend>Create your account</legend>
                            
                                <?= $this->Form->input('username', array('id' => 'add-user','class'=> 'add-user','placeholder' => 'Username must be between 5 and 32 characters', 'pattern' => "^[a-z0-9]{5,32}$", 'required' => 'required', 'autofocus')) ?>
                            
                                <?= $this->Form->input('password', array('id' => 'add-user','class'=> 'add-user','placeholder' => '8-32 characters, at least one number and one uppercase letter','pattern' => "(?=.*\d)(?=.*[A-Z]).{8,32}", 'required' => 'required')); ?>
                                <?= $this->Form->input('confirm_password', array('type'=>'password','placeholder'=>'Type your password again!')); ?>

                                <?= $this->Form->input('first_name', array('placeholder' => 'Firstname', 'required' => 'required')); ?>
                            
                                <?= $this->Form->input('last_name', array('placeholder' => 'Lastname', 'required' => 'required')); ?>
                                <?= $this->Form->input('avatar', array('type' => 'file')); ?>
                            
                                <?= $this->Form->input('email', array('type' => 'email','placeholder' => 'Email', 'required' => 'required')); ?>
                                <label>Birth:</label>
                                <?= $this->Form->input('birth', array(  
                                                                        'type' => 'date',
                                                                        'label' => false,
                                                                        'dateFormat' => 'YMD', 
                                                                        'minYear' => date('Y') - 150, 
                                                                        'maxYear' => date('Y') + 0)
                                                                    ); ?>
                            
                                <?= $this->Form->input('homephone', array('type' => 'Number','placeholder' => 'Homephone')); ?>
                            
                                <?= $this->Form->input('mobilephone', array('type' => 'Number','placeholder' => 'Mobilephone')); ?>
                            
                                <?= $this->Form->input('address', array('placeholder' => 'Address')); ?>
                            
                            <?= $this->Form->button('Register Now', array('type' => 'submit')); ?>
                            <?= $this->Form->button('Reset', array('type' => 'reset')); ?>
                        </fieldset>
                    <?= $this->Form->end();  ?>
            </div>    
    </article>
    </div>
</section>
<script>
    
</script>