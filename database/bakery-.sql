-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 04, 2016 at 08:42 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bakery`
--

-- --------------------------------------------------------

--
-- Table structure for table `acos`
--

CREATE TABLE `acos` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT '',
  `foreign_key` int(10) UNSIGNED DEFAULT NULL,
  `alias` varchar(255) DEFAULT '',
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acos`
--

INSERT INTO `acos` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, NULL, NULL, 'controllers', 1, 326),
(2, 1, NULL, NULL, 'Categories', 2, 17),
(3, 2, NULL, NULL, 'index', 3, 4),
(4, 2, NULL, NULL, 'view', 5, 6),
(5, 2, NULL, NULL, 'add', 7, 8),
(6, 2, NULL, NULL, 'delete', 9, 10),
(7, 2, NULL, NULL, 'sum', 11, 12),
(8, 2, NULL, NULL, 'between', 13, 14),
(9, 1, NULL, NULL, 'Coupons', 18, 31),
(10, 9, NULL, NULL, 'index', 19, 20),
(11, 9, NULL, NULL, 'view', 21, 22),
(12, 9, NULL, NULL, 'add', 23, 24),
(13, 9, NULL, NULL, 'sum', 25, 26),
(14, 9, NULL, NULL, 'between', 27, 28),
(15, 1, NULL, NULL, 'Faqs', 32, 49),
(16, 15, NULL, NULL, 'index', 33, 34),
(17, 15, NULL, NULL, 'view', 35, 36),
(18, 15, NULL, NULL, 'add', 37, 38),
(19, 15, NULL, NULL, 'delete', 39, 40),
(20, 15, NULL, NULL, 'edit', 41, 42),
(21, 15, NULL, NULL, 'sum', 43, 44),
(22, 15, NULL, NULL, 'between', 45, 46),
(23, 1, NULL, NULL, 'Groups', 50, 61),
(24, 23, NULL, NULL, 'sum', 51, 52),
(25, 23, NULL, NULL, 'between', 53, 54),
(26, 1, NULL, NULL, 'Orders', 62, 73),
(27, 26, NULL, NULL, 'checkout', 63, 64),
(28, 26, NULL, NULL, 'order', 65, 66),
(29, 26, NULL, NULL, 'sum', 67, 68),
(30, 26, NULL, NULL, 'between', 69, 70),
(31, 1, NULL, NULL, 'Pages', 74, 91),
(32, 31, NULL, NULL, 'home', 75, 76),
(33, 31, NULL, NULL, 'about_us', 77, 78),
(34, 31, NULL, NULL, 'quality', 79, 80),
(35, 31, NULL, NULL, 'shopping_guide', 81, 82),
(36, 31, NULL, NULL, 'contact', 83, 84),
(37, 31, NULL, NULL, 'sum', 85, 86),
(38, 31, NULL, NULL, 'between', 87, 88),
(39, 1, NULL, NULL, 'Products', 92, 123),
(40, 39, NULL, NULL, 'index', 93, 94),
(41, 39, NULL, NULL, 'delete', 95, 96),
(42, 39, NULL, NULL, 'detail', 97, 98),
(43, 39, NULL, NULL, 'category', 99, 100),
(44, 39, NULL, NULL, 'check_out', 101, 102),
(45, 39, NULL, NULL, 'quickviewP', 103, 104),
(46, 39, NULL, NULL, 'search_product', 105, 106),
(47, 39, NULL, NULL, 'add_to_cart', 107, 108),
(48, 39, NULL, NULL, 'update_cart', 109, 110),
(49, 39, NULL, NULL, 'remove', 111, 112),
(50, 39, NULL, NULL, 'empty_cart', 113, 114),
(51, 39, NULL, NULL, 'search', 115, 116),
(52, 39, NULL, NULL, 'sum', 117, 118),
(53, 39, NULL, NULL, 'between', 119, 120),
(54, 1, NULL, NULL, 'Users', 124, 143),
(55, 54, NULL, NULL, 'index', 125, 126),
(56, 54, NULL, NULL, 'login', 127, 128),
(57, 54, NULL, NULL, 'logout', 129, 130),
(58, 54, NULL, NULL, 'register', 131, 132),
(59, 54, NULL, NULL, 'edit', 133, 134),
(60, 54, NULL, NULL, 'delete', 135, 136),
(61, 54, NULL, NULL, 'sum', 137, 138),
(62, 54, NULL, NULL, 'between', 139, 140),
(63, 1, NULL, NULL, 'AclExtras', 144, 145),
(64, 1, NULL, NULL, 'Admin', 146, 311),
(65, 64, NULL, NULL, 'Categories', 147, 162),
(66, 65, NULL, NULL, 'index', 148, 149),
(67, 65, NULL, NULL, 'view', 150, 151),
(68, 65, NULL, NULL, 'add', 152, 153),
(69, 65, NULL, NULL, 'delete', 154, 155),
(70, 65, NULL, NULL, 'sum', 156, 157),
(71, 65, NULL, NULL, 'between', 158, 159),
(72, 64, NULL, NULL, 'Coupons', 163, 176),
(73, 72, NULL, NULL, 'index', 164, 165),
(74, 72, NULL, NULL, 'view', 166, 167),
(75, 72, NULL, NULL, 'add', 168, 169),
(76, 72, NULL, NULL, 'sum', 170, 171),
(77, 72, NULL, NULL, 'between', 172, 173),
(78, 64, NULL, NULL, 'Faqs', 177, 194),
(79, 78, NULL, NULL, 'index', 178, 179),
(80, 78, NULL, NULL, 'view', 180, 181),
(81, 78, NULL, NULL, 'add', 182, 183),
(82, 78, NULL, NULL, 'delete', 184, 185),
(83, 78, NULL, NULL, 'edit', 186, 187),
(84, 78, NULL, NULL, 'sum', 188, 189),
(85, 78, NULL, NULL, 'between', 190, 191),
(86, 64, NULL, NULL, 'Groups', 195, 206),
(87, 86, NULL, NULL, 'sum', 196, 197),
(88, 86, NULL, NULL, 'between', 198, 199),
(89, 64, NULL, NULL, 'Pages', 207, 224),
(90, 89, NULL, NULL, 'home', 208, 209),
(91, 89, NULL, NULL, 'about_us', 210, 211),
(92, 89, NULL, NULL, 'quality', 212, 213),
(93, 89, NULL, NULL, 'shopping_guide', 214, 215),
(94, 89, NULL, NULL, 'contact', 216, 217),
(95, 89, NULL, NULL, 'sum', 218, 219),
(96, 89, NULL, NULL, 'between', 220, 221),
(97, 64, NULL, NULL, 'Products', 225, 256),
(98, 97, NULL, NULL, 'index', 226, 227),
(99, 97, NULL, NULL, 'delete', 228, 229),
(100, 97, NULL, NULL, 'detail', 230, 231),
(101, 97, NULL, NULL, 'category', 232, 233),
(102, 97, NULL, NULL, 'check_out', 234, 235),
(103, 97, NULL, NULL, 'quickviewP', 236, 237),
(104, 97, NULL, NULL, 'search_product', 238, 239),
(105, 97, NULL, NULL, 'add_to_cart', 240, 241),
(106, 97, NULL, NULL, 'update_cart', 242, 243),
(107, 97, NULL, NULL, 'remove', 244, 245),
(108, 97, NULL, NULL, 'empty_cart', 246, 247),
(109, 97, NULL, NULL, 'search', 248, 249),
(110, 97, NULL, NULL, 'sum', 250, 251),
(111, 97, NULL, NULL, 'between', 252, 253),
(112, 64, NULL, NULL, 'Reviews', 257, 274),
(113, 112, NULL, NULL, 'index', 258, 259),
(114, 112, NULL, NULL, 'view', 260, 261),
(115, 112, NULL, NULL, 'add', 262, 263),
(116, 112, NULL, NULL, 'delete', 264, 265),
(117, 112, NULL, NULL, 'edit', 266, 267),
(118, 112, NULL, NULL, 'sum', 268, 269),
(119, 112, NULL, NULL, 'between', 270, 271),
(120, 64, NULL, NULL, 'Settings', 275, 290),
(121, 120, NULL, NULL, 'index', 276, 277),
(122, 120, NULL, NULL, 'add', 278, 279),
(123, 120, NULL, NULL, 'delete', 280, 281),
(124, 120, NULL, NULL, 'edit', 282, 283),
(125, 120, NULL, NULL, 'sum', 284, 285),
(126, 120, NULL, NULL, 'between', 286, 287),
(127, 64, NULL, NULL, 'Users', 291, 310),
(128, 127, NULL, NULL, 'index', 292, 293),
(129, 127, NULL, NULL, 'login', 294, 295),
(130, 127, NULL, NULL, 'logout', 296, 297),
(131, 127, NULL, NULL, 'register', 298, 299),
(132, 127, NULL, NULL, 'edit', 300, 301),
(133, 127, NULL, NULL, 'delete', 302, 303),
(134, 127, NULL, NULL, 'sum', 304, 305),
(135, 127, NULL, NULL, 'between', 306, 307),
(136, 2, NULL, NULL, 'isAuthorized', 15, 16),
(137, 9, NULL, NULL, 'isAuthorized', 29, 30),
(138, 15, NULL, NULL, 'isAuthorized', 47, 48),
(139, 23, NULL, NULL, 'isAuthorized', 55, 56),
(140, 26, NULL, NULL, 'isAuthorized', 71, 72),
(141, 31, NULL, NULL, 'isAuthorized', 89, 90),
(142, 39, NULL, NULL, 'isAuthorized', 121, 122),
(143, 54, NULL, NULL, 'isAuthorized', 141, 142),
(144, 65, NULL, NULL, 'isAuthorized', 160, 161),
(145, 72, NULL, NULL, 'isAuthorized', 174, 175),
(146, 78, NULL, NULL, 'isAuthorized', 192, 193),
(147, 86, NULL, NULL, 'isAuthorized', 200, 201),
(148, 89, NULL, NULL, 'isAuthorized', 222, 223),
(149, 97, NULL, NULL, 'isAuthorized', 254, 255),
(150, 112, NULL, NULL, 'isAuthorized', 272, 273),
(151, 120, NULL, NULL, 'isAuthorized', 288, 289),
(152, 127, NULL, NULL, 'isAuthorized', 308, 309),
(153, 1, NULL, NULL, 'DebugKit', 312, 325),
(154, 153, NULL, NULL, 'ToolbarAccess', 313, 324),
(155, 154, NULL, NULL, 'history_state', 314, 315),
(156, 154, NULL, NULL, 'sql_explain', 316, 317),
(157, 154, NULL, NULL, 'isAuthorized', 318, 319),
(158, 154, NULL, NULL, 'sum', 320, 321),
(159, 154, NULL, NULL, 'between', 322, 323),
(160, 23, NULL, NULL, 'admin_instll', 57, 58),
(161, 86, NULL, NULL, 'admin_instll', 202, 203),
(162, 23, NULL, NULL, 'admin_install', 59, 60),
(163, 86, NULL, NULL, 'admin_install', 204, 205);

-- --------------------------------------------------------

--
-- Table structure for table `aros`
--

CREATE TABLE `aros` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT '',
  `foreign_key` int(10) UNSIGNED DEFAULT NULL,
  `alias` varchar(255) DEFAULT '',
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aros`
--

INSERT INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, 'Group', 1, 'Admin', 1, 36),
(2, NULL, 'Group', 2, 'User', 37, 66),
(3, NULL, 'Group', 3, 'Banned', 67, 70),
(33, 1, 'User', 72, '', 32, 33),
(34, 1, 'User', 73, '', 34, 35),
(35, 2, 'User', 74, '', 64, 65);

-- --------------------------------------------------------

--
-- Table structure for table `aros_acos`
--

CREATE TABLE `aros_acos` (
  `id` int(10) UNSIGNED NOT NULL,
  `aro_id` int(10) UNSIGNED NOT NULL,
  `aco_id` int(10) UNSIGNED NOT NULL,
  `_create` char(2) NOT NULL DEFAULT '0',
  `_read` char(2) NOT NULL DEFAULT '0',
  `_update` char(2) NOT NULL DEFAULT '0',
  `_delete` char(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aros_acos`
--

INSERT INTO `aros_acos` (`id`, `aro_id`, `aco_id`, `_create`, `_read`, `_update`, `_delete`) VALUES
(2, 2, 1, '1', '1', '1', '1'),
(3, 2, 64, '-1', '-1', '-1', '-1');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) NOT NULL,
  `name` varchar(250) NOT NULL,
  `image` varchar(250) NOT NULL,
  `info` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `image`, `info`) VALUES
(2, 'ice cream cake', 'category1.png', 'You see the hedges, how I got it shaped up? It''s important to shape up your hedges, it''s like getting a haircut, stay fresh'),
(3, 'donut cream', 'category2.png', 'You see the hedges, how I got it shaped up? It''s important to shape up your hedges, it''s like getting a haircut, stay fresh'),
(4, 'bread', 'category3.png', 'You see the hedges, how I got it shaped up? It''s important to shape up your hedges, it''s like getting a haircut, stay fresh'),
(5, 'bakery', 'category4.png', 'You see the hedges, how I got it shaped up? It''s important to shape up your hedges, it''s like getting a haircut, stay fresh'),
(6, 'coffee', 'category5.png', 'You see the hedges, how I got it shaped up? It''s important to shape up your hedges, it''s like getting a haircut, stay fresh');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `percent` int(3) NOT NULL,
  `description` text NOT NULL,
  `time_start` datetime NOT NULL,
  `time_end` datetime NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `code`, `percent`, `description`, `time_start`, `time_end`, `created`, `modified`) VALUES
(1, 'COUPON01-2104801023', 20, 'Discount 20% for all product in cart!', '2016-07-07 00:00:00', '2016-09-15 00:00:00', '2016-07-13 08:29:46', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(141) NOT NULL,
  `question` varchar(250) NOT NULL,
  `answer` text NOT NULL,
  `approve` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `question`, `answer`, `approve`) VALUES
(1, 'which category of bakery from store?', 'The other day the grass was brown, now it''s green because I ain''t give up. Never surrender. The key to more success is to have a lot of pillows. Egg whites, turkey sausage, wheat toast, water. Of course they don''t want us to eat our breakfast, so we are going to enjoy our breakfast.', 1),
(2, 'delivery and shipping policy', 'The other day the grass was brown, now it''s green because I ain''t give up. Never surrender. The key to more success is to have a lot of pillows. Egg whites, turkey sausage, wheat toast, water. Of course they don''t want us to eat our breakfast, so we are going to enjoy our breakfast.', 1),
(3, 'HOW TO GET A REFUND?', 'The other day the grass was brown, now it''s green because I ain''t give up. Never surrender. The key to more success is to have a lot of pillows. Egg whites, turkey sausage, wheat toast, water. Of course they don''t want us to eat our breakfast, so we are going to enjoy our breakfast.', 1),
(4, 'opening hour?', 'The other day the grass was brown, now it''s green because I ain''t give up. Never surrender. The key to more success is to have a lot of pillows. Egg whites, turkey sausage, wheat toast, water. Of course they don''t want us to eat our breakfast, so we are going to enjoy our breakfast.', 1),
(5, 'Term & conditions', 'The other day the grass was brown, now it''s green because I ain''t give up. Never surrender. The key to more success is to have a lot of pillows. Egg whites, turkey sausage, wheat toast, water. Of course they don''t want us to eat our breakfast, so we are going to enjoy our breakfast.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`, `created`) VALUES
(1, 'Admin', 'Admin', NULL),
(2, 'User', 'member', NULL),
(3, 'Banned', 'banned user', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `customer_info` text NOT NULL,
  `order_info` text NOT NULL,
  `payment_info` text NOT NULL,
  `status` int(11) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `customer_info`, `order_info`, `payment_info`, `status`, `created`, `modified`) VALUES
(12, NULL, '{"name":"c\\u00e1","phone":"dsa","adress":"ddddddddddddddddddd"}', '{"2":{"id":"2","name":"Americano Coffee","image":"products_img2.jpg","rating":"5","price":"15.00","sale_off":"29","sale_start":"2016-07-03 00:00:00","sale_end":"2016-08-30 00:00:00","prices":10.65,"quantity":1},"3":{"id":"3","name":"Eleifend option congue","image":"products_img3.jpg","rating":"3","price":"15.00","sale_off":"10","sale_start":"2016-07-04 00:00:00","sale_end":"2016-10-19 00:00:00","prices":13.5,"quantity":1}}', '{"quantity_":2,"total":24.15}', 0, '2016-07-13 13:58:06', '2016-07-13 13:58:06'),
(13, 9, 'null', '{"1":{"id":"1","name":"Mirum est notare littera","image":"products_img1.jpg","rating":"5","price":"15.00","sale_off":"10","sale_start":"2016-07-04 00:00:00","sale_end":"2016-06-16 00:00:00","prices":"15.00","quantity":1},"2":{"id":"2","name":"Americano Coffee","image":"products_img2.jpg","rating":"5","price":"15.00","sale_off":"29","sale_start":"2016-07-03 00:00:00","sale_end":"2016-08-30 00:00:00","prices":10.65,"quantity":1}}', '{"quantity_":2,"total":25.65,"coupon":"COUPON01-2104801023","discount":"20","pay":20.52}', 0, '2016-07-14 04:56:00', '2016-07-14 04:56:00');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `path_img1` varchar(250) NOT NULL,
  `path_img2` varchar(250) NOT NULL,
  `path_img3` varchar(250) NOT NULL,
  `path_img4` varchar(250) NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `sale_off` decimal(5,0) NOT NULL,
  `sale_start` datetime NOT NULL,
  `sale_end` datetime DEFAULT NULL,
  `expiry` int(10) DEFAULT NULL,
  `new` tinyint(1) NOT NULL,
  `recommended` tinyint(1) NOT NULL,
  `cold-storage` tinyint(1) NOT NULL,
  `rating` int(1) NOT NULL,
  `available` varchar(100) NOT NULL,
  `references` char(20) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `name`, `description`, `path_img1`, `path_img2`, `path_img3`, `path_img4`, `price`, `sale_off`, `sale_start`, `sale_end`, `expiry`, `new`, `recommended`, `cold-storage`, `rating`, `available`, `references`, `published`, `created`, `updated`) VALUES
(1, 2, 'Mirum est notare littera', 'Watch your back, but more\r\nimportantly when you get out the\r\nshower, dry your...', 'products_img1.jpg', 'products_img1.jpg', 'products_img1.jpg', 'products_img1.jpg', '15.00', '10', '2016-07-04 00:00:00', '2016-06-16 00:00:00', 7, 1, 1, 0, 5, 'stoped', 'mirum1223', 1, '2016-07-14 09:12:19', '2016-06-26 21:10:41'),
(2, 6, 'Americano Coffee', 'Watch your back, but more\r\nimportantly when you get out the\r\nshower, dry your...', 'products_img2.jpg', 'products_img2.jpg', 'products_img2.jpg', 'products_img2.jpg', '15.00', '29', '2016-07-03 00:00:00', '2016-08-30 00:00:00', 6, 0, 0, 0, 5, '', '', 1, '2016-07-12 03:25:24', '2016-06-22 21:16:47'),
(3, 4, 'Eleifend option congue', 'Watch your back, but more\r\nimportantly when you get out the\r\nshower, dry your...', 'products_img3.jpg', 'products_img3_1.jpg', 'products_img3_3.jpg', 'products_img3_4.jpg', '15.00', '10', '2016-07-04 00:00:00', '2016-10-19 00:00:00', 7, 0, 0, 0, 3, '', '', 0, '2016-07-12 03:25:13', '2016-06-22 21:16:55'),
(4, 6, 'Americano Coffee', 'Watch your back, but more\r\nimportantly when you get out the\r\nshower, dry your...', 'products_img4.jpg', 'products_img2.jpg', 'products_img4.jpg', 'products_img4.jpg', '11.00', '0', '0000-00-00 00:00:00', '2016-06-23 00:00:00', 7, 1, 1, 0, 3, '', '', 1, '2016-07-12 03:25:35', '2016-06-26 21:28:51'),
(5, 3, 'Eleifend option congue', 'Watch your back, but more\r\nimportantly when you get out the\r\nshower, dry your...', 'products_img5.jpg', '', '', '', '12.00', '0', '0000-00-00 00:00:00', '2016-06-23 00:00:00', 7, 0, 0, 0, 4, '', '', 0, '2016-07-08 06:45:27', '2016-06-22 21:17:18'),
(6, 4, 'Nunc nobis videntur parum', 'Watch your back, but more\r\nimportantly when you get out the\r\nshower, dry your...', 'products_img6.jpg', '', '', '', '15.00', '29', '2016-07-05 00:00:00', '2016-07-27 00:00:00', 7, 0, 0, 0, 5, '', '', 0, '2016-07-08 06:45:31', '2016-06-22 22:21:21'),
(7, 4, 'Mirum est notare littera', 'Watch your back, but more\r\nimportantly when you get out the\r\nshower, dry your...', 'products_img7.jpg', '', '', '', '11.00', '0', '0000-00-00 00:00:00', '2016-06-23 00:00:00', 7, 0, 0, 0, 4, '', '', 0, '2016-07-08 06:45:30', '2016-06-22 21:17:37'),
(8, 5, 'Mirum est notare quam gothica', 'Watch your back, but more\r\nimportantly when you get out the\r\nshower, dry your...', 'products_img8.jpg', '', '', '', '11.00', '0', '0000-00-00 00:00:00', '2016-06-23 00:00:00', 7, 0, 0, 0, 5, '', '', 0, '2016-07-08 06:45:33', '2016-06-22 21:17:45'),
(9, 5, 'Mirum est notare littera', 'Watch your back, but more\r\nimportantly when you get out the\r\nshower, dry your...', 'products_img9.jpg', '', '', '', '11.00', '0', '0000-00-00 00:00:00', '2016-06-23 00:00:00', 7, 0, 0, 0, 4, '', '', 0, '2016-07-08 06:45:35', '2016-06-22 21:17:57');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `review` text NOT NULL,
  `approve` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `user_id`, `product_id`, `review`, `approve`, `created`, `updated`) VALUES
(1, 1, 1, 'Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. MiruLorem Khaled Ipsum is a major key to success. Another one. Always remember in the jungle there’s a lot of they in there, after you overcome they, you will make it to paradise. The key to more success is to have a lot of pillows. The ladies always say Khaled you smell good, I use no cologne. Cocoa butter is the key. Let’s see what Chef Dee got that they don’t want us to eat. Stay focused. Cloth talk. They key is to have every key, the key to open every door. We the best. The key to success is to keep your head above the water, never give up. Special cloth alert. Celebrate success right, the only way, apple.m est notare quam littera gothica, quam nunc putamus parum anteposuerit', 1, '2016-05-30 08:03:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `value` text NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`, `updated`) VALUES
(1, 'site_name', 'bakery store', '2016-07-15 01:53:05'),
(2, 'site_logo', 'bakery.', '2016-05-30 04:51:08'),
(3, 'contact_mail', 'info@yourdomain.com', '2016-05-30 04:53:26'),
(4, 'contact_address', '221b Baker Street - London - United Kingdom', '2016-05-30 04:53:26'),
(5, 'contact_phone1', '0123.456.789', '2016-05-30 06:40:04'),
(6, 'contact_phone2', '0123.987.654', '2016-05-30 06:40:04'),
(7, 'payment_visa', '1234 1234 1234 1234', '2016-05-30 06:46:53'),
(8, 'payment_mastercard', '1234 1234 1234 1234', '2016-05-30 06:46:53'),
(9, 'payment_paypal', '1234 1234 1234 1234', '2016-05-30 06:47:45'),
(10, 'payment_americanexpress', '1234 1234 1234 1234', '2016-05-30 06:47:45'),
(11, 'payment_maestro', '1234 1234 1234 1234', '2016-05-30 06:49:00'),
(12, 'delivery_shipping', 'You should never complain, complaining is a weak emotion, you got life, we breathing, we blessed. Always remember in the jungle there''s a lot of they in there, after you overcome they, you will make it to paradise. You smart, you loyal, you a genius.', '2016-05-30 06:49:00'),
(13, 'shoping_guide', '1. Choose the section and then the product type\r\n\r\n2. Select an item of clothing and add it to your basket. You can then choose to continue shopping or to process order\r\n\r\n3. Select a payment method: Visa, Visa Electron, Mastercard, American Express, PayPal, Gift Card...\r\n\r\n4. Confirm the order.\r\n\r\n5. You will receive an email confirming your order', '2016-05-30 06:51:10'),
(14, 'return_policy', 'In store: The easiest way to return an item is by taking it to one of our website\r\n\r\nHome collection/Drop off: Please log in and follow the steps listed in my account > returns. If you are not registered at Zara.com, please use the link sent to you with your order confirmation e-mail.\r\n\r\nPlease note that it is always necessary to have received the merchandise prior to requesting a refund.', '2016-05-30 06:51:10'),
(15, 'Open_hour', '8 am', '2016-07-06 00:08:06'),
(16, 'Close_hour', '10 pm', '2016-07-06 00:09:14');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL DEFAULT '2',
  `username` varchar(32) NOT NULL,
  `password` varchar(64) NOT NULL,
  `first_name` varchar(32) NOT NULL,
  `last_name` varchar(32) NOT NULL,
  `avatar` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `birth` date DEFAULT NULL,
  `homephone` varchar(20) NOT NULL,
  `mobilephone` varchar(20) NOT NULL,
  `address` varchar(250) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `group_id`, `username`, `password`, `first_name`, `last_name`, `avatar`, `email`, `birth`, `homephone`, `mobilephone`, `address`, `created`) VALUES
(72, 1, 'admin', '$2a$10$jhHzMqXtoMhb5uk9uM7R4.7rxE3HOcNAEsO7K67bByBjQkDreeLyy', 'Nguyen', 'Nghiep', '', 'nghiep@gmail.com', '2016-08-04', '1330456789', '12314656547897', '123abc', '2016-08-03 23:35:19'),
(73, 2, 'admin1', '$2a$10$CkG3RYCkrsgHQ6b8.x2JdO/8tHSYGw.GXeJu4GgRh0o1k5Dk5E7qO', 'Nguyen', 'Nghiep', '', 'nghiep@gmail.com', '2016-08-04', '', '', '', '2016-08-04 04:42:56'),
(74, 2, 'banhmy', '$2a$10$8FMdhErV6cTejZGfVY0YRO/iDKsDusXK.pyxOOtuQSEkxcum7JvxG', 'Nguyen', 'Nghiep', '', 'nghiep@gmail.com', '2016-08-04', '', '', '', '2016-08-04 01:37:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acos`
--
ALTER TABLE `acos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_acos_lft_rght` (`lft`,`rght`),
  ADD KEY `idx_acos_alias` (`alias`);

--
-- Indexes for table `aros`
--
ALTER TABLE `aros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_aros_lft_rght` (`lft`,`rght`),
  ADD KEY `idx_aros_alias` (`alias`);

--
-- Indexes for table `aros_acos`
--
ALTER TABLE `aros_acos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_aco_id` (`aco_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acos`
--
ALTER TABLE `acos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=164;
--
-- AUTO_INCREMENT for table `aros`
--
ALTER TABLE `aros`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `aros_acos`
--
ALTER TABLE `aros_acos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(141) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
